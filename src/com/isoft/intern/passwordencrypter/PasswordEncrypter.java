package com.isoft.intern.passwordencrypter;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * User: Nikolay Uzunov
 * This class serves to encrypt a string.
 */
public class PasswordEncrypter
{
    /**
     * User: Nikolay Uzunov
     * This passwor serves for encrypting a password.
     * @param password string for encryption.
     */
    public static String encryptPassword(String password)
    {
        MessageDigest digest = null;
        try
        {
            String pass = password;
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(pass.getBytes());
            String encodedPassword = Base64.getEncoder().encodeToString(hash);
            return encodedPassword;
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        return null;
    }

}
