package com.isoft.intern.services.api;

import com.isoft.intern.exceptions.UserLockedException;
import com.isoft.intern.model.User;

import java.util.List;

/**
 * @author Nikolay Uzunov
 *
 */
public interface UserService
{
    void insertUser(User user);
    List<User> getAllUsers();
    void updateUser(User user);
    boolean usernameExists(String username);
    boolean emailExists(String email);
    boolean login(String usernameOrEmail, String password) throws UserLockedException;
    void deleteUser(User user);
}
