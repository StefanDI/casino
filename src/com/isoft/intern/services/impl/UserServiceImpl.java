package com.isoft.intern.services.impl;

import com.isoft.intern.config.Config;
import com.isoft.intern.exceptions.UserLockedException;
import com.isoft.intern.io.FileInputReader;
import com.isoft.intern.io.FileOutputWriter;
import com.isoft.intern.model.User;
import com.isoft.intern.model.UserMapper;
import com.isoft.intern.model.UserRoles;
import com.isoft.intern.passwordencrypter.PasswordEncrypter;
import com.isoft.intern.repositories.impl.UserRepositoryImpl;
import com.isoft.intern.services.api.UserService;
import com.isoft.intern.userholder.LoggedInUser;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Nikolay Uzunov
 * Тhis class serves for communication between repository and views.
 * Contains buisness loging for User object.
 */
public class UserServiceImpl extends Observable implements UserService
{
    public static final String EMAILS = "emails";
    public static final String USERNAMES = "usernames";
    private UserRepositoryImpl userRepository;
    private FileInputReader fileInputReader;
    private static final Logger LOGGER = Logger.getGlobal();
    private String lastLoginAttempt;
    private int wrongLoginAttempts = 0;

    public UserServiceImpl()
    {
        addObserver(LoggedInUser.getInstance());
        try
        {
            fileInputReader = new FileInputReader(UserRepositoryImpl.LOGIN_DATA_PATH);
            userRepository = new UserRepositoryImpl(new FileOutputWriter(UserRepositoryImpl.LOGIN_DATA_PATH), fileInputReader);
        }
        catch (FileNotFoundException e)
        {
            LOGGER.log(Level.WARNING, "File not found!", e);
        }
        catch (IOException e)
        {
            LOGGER.log(Level.WARNING, "Couldn't read a file!", e);
        }
    }

    /**
     * This method serialized the User object to JSONObject in String format and send it to UserRepository.
     * Method must be passed valid user.
     *
     * @param user User object
     */
    @Override
    public void insertUser(User user)
    {
        try
        {
            JSONObject loginDataJson = userRepository.getAllLoginData();
            if (loginDataJson != null)
            {
                String encryptPassword = PasswordEncrypter.encryptPassword(user.getPassword());
                user.setPassword(encryptPassword);
                JSONObject usernamesJson = (JSONObject) loginDataJson.get(USERNAMES);
                usernamesJson.put(user.getUsername(), encryptPassword);
                JSONObject emailsJson = (JSONObject) loginDataJson.get(EMAILS);
                emailsJson.put(user.getEmail(), user.getUsername());
                loginDataJson.put(USERNAMES, usernamesJson);
                loginDataJson.put(EMAILS, emailsJson);
                JSONObject jsonUser = new JSONObject(UserMapper.serialize(user));
                userRepository.insert(jsonUser);
                userRepository.updateLoginData(loginDataJson.toString());
            }
        }
        catch (IOException e)
        {
            LOGGER.log(Level.WARNING, "Couldn't read a file!", e);
        }
    }

    /**
     * Тhis method serves to get all users.
     */
    @Override
    public List<User> getAllUsers()
    {
        List<User> users = new ArrayList<>();
        for (JSONObject user : userRepository.getAll())
        {
            users.add(UserMapper.deserialize(user));
        }
        return users;
    }

    /**
     * Тhis method serves to check if there is such an username.
     *
     * @param username holds User's username.
     */
    @Override
    public boolean usernameExists(String username)
    {
        return validateLogin(username, USERNAMES);
    }

    /**
     * Тhis method serves to check if there is such an email.
     *
     * @param email holds User's email.
     */
    @Override
    public boolean emailExists(String email)
    {
        return validateLogin(email, EMAILS);
    }

    /**
     * Тhis method serves to update user's data.
     *
     * @param user
     */
    @Override
    public void updateUser(User user)
    {
        new Thread(()->{
            if (LoggedInUser.getInstance().getCurrentlyLoggedUser().getUsername().equals(user.getUsername()))
            {
                updateLoggedInUser(user);
            }
            JSONObject userJson = new JSONObject(UserMapper.serialize(user));
            try
            {
                updateLoginData(user);
                userRepository.update(userJson);
                LOGGER.log(Level.INFO, "Operation Update is successful!");
            }
            catch (IOException e)
            {
                LOGGER.log(Level.WARNING, " Operation insert is failed! Couldn't write in a file!", e);
            }
        }).start();
    }
    /**
     * Тhis method serves to update login data.
     *
     * @param user holds User's entity.
     */
    private void updateLoginData(User user)
    {
        JSONObject emailsJson = (JSONObject) userRepository.getAllLoginData().get(EMAILS);
        JSONObject usernamesJson = (JSONObject) userRepository.getAllLoginData().get(USERNAMES);
        updateEmailsList(user, emailsJson);
        updateUsernamesList(user, usernamesJson);
        updatePasswordList(user, usernamesJson);
    }

    /**
     * Тhis method serves to check if an account exists.
     *
     * @param usernameOrEmail holds User's username or password.
     * @param password holds User's password.
     */
    @Override
    public boolean login(String usernameOrEmail, String password) throws UserLockedException
    {
        try
        {
            if (usernameOrEmail.contains("@"))
            {
                return emailLogin(usernameOrEmail, password);
            }
            else
            {
                return usernameLogin(usernameOrEmail, password);
            }
        }
        catch (JSONException e)
        {
            LOGGER.log(Level.SEVERE, "Couldn,t deserialize object may be missing attributes in JSON file.", e);
            return false;
        }
    }

    /**
     * This method serves to delete user.
     *
     * @param user holds the user with changed attributes.
     */
    @Override
    public void deleteUser(User user)
    {
        if (user.getUserRole().equals(UserRoles.ADMIN))
        {
            JSONObject loginData = userRepository.getAllLoginData();
            if (loginData != null)
            {
                JSONObject emailsJson = (JSONObject) loginData.get(EMAILS);
                JSONObject usernamesJson = (JSONObject) loginData.get(USERNAMES);
                emailsJson.remove(user.getEmail());
                usernamesJson.remove(user.getUsername());
                loginData.put(EMAILS, emailsJson);
                loginData.put(USERNAMES, usernamesJson);
            }
            userRepository.delete(new JSONObject(UserMapper.serialize(user)));
            userRepository.updateLoginData(loginData.toString());
        }
    }

    /**
     * This method serves to set changes on logged in user using Observable.
     * So the logged-in user is always up to date.
     *
     * @param user holds the user with changed attributes.
     */
    private void updateLoggedInUser(User user)
    {
        //Observer pattern
        //The Observable(this UserServiceImpl)
        //Tells all Observers (in our case just LoggedInUser class)
        //That something has changed (setChanged())
        //AND notifyObservers about what is changed (updated user)
        setChanged();
        notifyObservers(user);
    }

    /**
     * Тhis method implements the logic needed for emailEx {@link #emailExists(String)} and {@link #usernameExists(String)}.
     *
     * @param usernameOrEmail holds User's username or password.
     * @param loginType       holds login type (EMAILS \ USERNAMES).
     */
    private boolean validateLogin(String usernameOrEmail, String loginType)
    {
        JSONObject loginDataJson = userRepository.getAllLoginData();
        if (loginDataJson != null)
        {
            JSONObject emailsJson = (JSONObject) loginDataJson.get(loginType);
            for (String s : emailsJson.keySet())
            {
                if (s.equals(usernameOrEmail))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Тhis method serves to checks if the account exists
     * when the user try to login with username.
     *
     * @param username holds User's username.
     * @param password holds User's password.
     * @throws UserLockedException If user's account is blocked the method throw this exception.
     */
    private boolean usernameLogin(String username, String password) throws UserLockedException
    {
        lastLoginAttempt = username;
        JSONObject loginDataJson = userRepository.getAllLoginData();
        if (loginDataJson != null)
        {
            JSONObject usernamesJson = (JSONObject) loginDataJson.get(USERNAMES);
            String usersPassword = usernamesJson.getString(username);
            String encryptedPassword = PasswordEncrypter.encryptPassword(password);
            if (usersPassword == null)
            {
                LOGGER.warning("NO SUCH USER");
                return false;
            }
            if (usersPassword.equals(encryptedPassword))
            {
                JSONObject userJson = userRepository.getUserByUsername(username);
                User user = UserMapper.deserialize(userJson);
                if (user.isLocked())
                {
                    throw new UserLockedException();
                }
                setChanged();
                notifyObservers(user);
                return true;
            }
            else
            {
                lockTheUser(username);
            }
        }
        return false;
    }

    /**
     * Тhis method serves to checks if the account exists
     * when the user try to login with email.
     *
     * @param email    holds User's email.
     * @param password holds User's password.
     * @throws UserLockedException If user's account is blocked the method throw this exception.
     */
    private boolean emailLogin(String email, String password) throws UserLockedException
    {
        JSONObject loginDataJson = userRepository.getAllLoginData();

        if (loginDataJson != null)
        {
            JSONObject emailsJson = (JSONObject) loginDataJson.get(EMAILS);
            //getting username by email key in json.
            String username = emailsJson.getString(email);

            //if username doesn't contains
            if (username != null)
            {
                lastLoginAttempt = username;
                String existingPassword = ((JSONObject) loginDataJson.get(USERNAMES)).getString(username);
                String userPassword = PasswordEncrypter.encryptPassword(password);
                if (existingPassword.equals(userPassword))
                {
                    User user = UserMapper.deserialize(userRepository.getUserByUsername(username));
                    if (user.isLocked())
                    {
                        throw new UserLockedException();
                    }
                    setChanged();
                    notifyObservers(user);
                    return true;
                }
                else
                {
                    lockTheUser(username);
                }
            }
        }
        return false;
    }

    /**
     * This method serves to lock a user's account
     *
     * @param username holds User's username.
     */
    private void lockTheUser(String username)
    {
        User user = UserMapper.deserialize(userRepository.getUserByUsername(username));
        if (username.equals(lastLoginAttempt))
        {
            if (wrongLoginAttempts == Config.MAX_LOGIN_ATTEMPTS && !user.getUserRole().equals(UserRoles.ADMIN))
            {
                User lockedUser = UserMapper.deserialize(userRepository.getUserByUsername(username));
                this.updateUser(lockedUser);
            }
            wrongLoginAttempts++;
        }
        else
        {
            lastLoginAttempt = "";
            wrongLoginAttempts = 0;
        }
    }

    /**
     * This method serves to update email list in login data
     */
    private void updateEmailsList(User user, JSONObject emailsJson)
    {
        if (!emailsJson.has(user.getEmail()))
        {
            JSONObject loginDataJson = userRepository.getAllLoginData();
            if (loginDataJson != null)
            {
                emailsJson.remove(LoggedInUser.getInstance().getCurrentlyLoggedUser().getEmail());
                emailsJson.put(user.getEmail(), user.getUsername());
                loginDataJson.remove(EMAILS);
                loginDataJson.put(EMAILS, emailsJson);
                userRepository.updateLoginData(loginDataJson.toString());
            }
        }
    }

    /**
     * This method serves to update email list in login data.
     */
    private void updateUsernamesList(User user, JSONObject usernamesJson)
    {
        if (!usernamesJson.has(user.getUsername()))
        {
            JSONObject loginDataJson = userRepository.getAllLoginData();
            if (loginDataJson != null)
            {
                JSONObject emailsJson = (JSONObject) loginDataJson.get(EMAILS);
                emailsJson.put(user.getEmail(), user.getUsername());
                String oldUsername = LoggedInUser.getInstance().getCurrentlyLoggedUser().getUsername();
                String password = usernamesJson.getString(oldUsername);
                usernamesJson.put(user.getUsername(), password);
                usernamesJson.remove(oldUsername);
                loginDataJson.put(EMAILS, emailsJson);
                loginDataJson.put(USERNAMES, usernamesJson);
                userRepository.updateLoginData(loginDataJson.toString());
            }
        }
    }

    /**
     * This method serves to update password list in login data.
     */
    private void updatePasswordList(User user, JSONObject usernamesJson)
    {
        String password = usernamesJson.getString(user.getUsername());
        if (!password.equals(user.getPassword()))
        {
            JSONObject loginDataJson = userRepository.getAllLoginData();
            usernamesJson.put(user.getUsername(), user.getPassword());
            loginDataJson.put(USERNAMES, usernamesJson);
            userRepository.updateLoginData(loginDataJson.toString());
        }
    }
}
