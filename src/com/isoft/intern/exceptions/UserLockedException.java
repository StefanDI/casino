package com.isoft.intern.exceptions;


/**
 * User: Nikolay Uzunov
 *  * Exception is thrown when user account is locked.
 *  * exception is only thrown in {@link com.isoft.intern.services.impl.UserServiceImpl#login(String, String)}
 */
public class UserLockedException extends Exception
{
}
