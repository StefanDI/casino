package com.isoft.intern.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Extracts all locales from a given folder path
 * locales should be in format locale_{locale name}.json
 * example: locale_bg.json
 *
 * @author Stefan Ivanov
 */
public class LocaleParser
{

    private LocaleParser()
    {
    }

    /**
     * Returns a Map with all available locales
     * Key is the index for user option
     * Value is the locale name
     *
     * @return array with all the locales
     */
    public static Map<Integer, String> getLocales(String localesFolderPath)
    {
        Map<Integer, String> localesMap = new HashMap<>();
        File dir = new File(localesFolderPath);
        if (dir.isDirectory())
        {
            File[] allLocaleFiles = dir.listFiles();
            for (int i = 0; i < allLocaleFiles.length; i++)
            {
                File f = allLocaleFiles[i];
                //since locale filenames should be "locale_{LOCALE}.json
                //split on _ will result locale and {LOCALE}.json
                String localeWithFileExtension = f.getName().split("_")[1];
                //Replace the file extension (.json) with empty string, so only the locale is left
                String locale = localeWithFileExtension.replace(".json", "");
                localesMap.put(i, locale);
            }
        }

        return localesMap;
    }
}
