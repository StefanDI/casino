package com.isoft.intern.utils.gameloader;

/**
 * @author Nikolay Uzunov
 */
public class Loader
{
    public static void load()
    {
        for (int i = 0; i <= 200; i = i + 20)
        {
            int maxBareSize = 10; // 10unit for 100%
            int remainProcent = ((100 * i) / 200) / maxBareSize;
            char defaultChar = '-';
            String icon = "$";
            String bare = new String(new char[maxBareSize]).replace('\0', defaultChar) + "]";
            StringBuilder bareDone = new StringBuilder();
            bareDone.append("[");
            for (int j = 0; j < remainProcent; j++)
            {
                bareDone.append(icon);
            }
            String bareRemain = bare.substring(remainProcent);
            System.out.print("\r" + bareDone + bareRemain + " " + remainProcent * 10 + "%");
            try
            {
                Thread.sleep(250);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        System.out.println("\n");
    }
}
