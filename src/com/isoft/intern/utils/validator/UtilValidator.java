package com.isoft.intern.utils.validator;

/**
 * @author Petar Hristov
 */
public class UtilValidator {

    /**
     * checks if the input is a number
     *
     * @param number the input
     * @return true if it matches the regex
     */
    public static boolean isNumber(String number)
    {
        return number.matches("[0-9.]+");
    }

    /**
     * checks if the input contains letters
     * @param name the input
     * @return true if it matches the regex
     */
    public static boolean nameContainsValidCharacters(String name)
    {
        return name.matches("[a-zA-Z]+");
    }

    /**
     *	checks if the input is double and its between 0-1000
     * @param number the input
     * @return
     */
    public static boolean isDoubleMoney(String number)
    {
        try
        {
            double money = Double.parseDouble(number);
            if(money < 0d || money > 1000d)
                return false;
        } catch(NumberFormatException e)
        {
            return false;
        }
        return true;
    }
}
