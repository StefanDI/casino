package com.isoft.intern.utils.validator;

/**
 * @author Petar Hristov
 */

public class UserValidator 
{
	
	private UserValidator() {}

    /**
     * checks if the input is between 3 and 32 chars
     *
     * @param stringToCheck
     * @return
     */
	public static boolean isValidSize(String stringToCheck) 
	{
        return stringToCheck.length() >= 3 && stringToCheck.length() < 32;
    }

    /**
     * checks if the input contains letters and numbers only by verifying with regex
     * @param stringToCheck
	 * @return
	 */
	public static boolean containsValidSymbols(String stringToCheck)
	{
		return stringToCheck.matches("[a-zA-Z0-9_]+");
    }

    /**
     * checks if the input length is between 5-32
     * @param password
	 * @return
	 */
	public static boolean passwordIsValidSize(String password) 
	{
		return password.length() > 4 && password.length() < 32;
    }

    /**
     * checks if the input is 18 or more
     * @param age
	 * @return
	 */
	public static boolean isLegalAge(String age) 
	{
		return Integer.parseInt(age) > 17;
    }

    /**
     * checks if the email is valid by regex validation
     * @param email
	 * @return
	 */
	public static boolean isValidEmail(String email) 
	{
		return email.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$");
	}
}
