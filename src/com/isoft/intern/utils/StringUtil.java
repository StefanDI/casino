package com.isoft.intern.utils;

/**
 * @author Stefan Ivanov
 */
@SuppressWarnings("Duplicates")
public class StringUtil
{
    private StringUtil()
    {

    }

    public static void printInline(int indentBetweenStrings, String... strings)
    {
        String indent = "";
        for (int i = 0; i < indentBetweenStrings; i++)
        {
            indent += " ";
        }
        StringBuilder sb = new StringBuilder();

        String[][] stringsToMatrix = new String[strings.length][];
        for (int i = 0; i < strings.length; i++)
        {
            try
            {
                stringsToMatrix[i] = strings[i].split("\n");
            } catch (NullPointerException onlyGodCanJudgeMe)
            {
            }
        }

        printInline(indent, sb, stringsToMatrix);

        System.out.println(sb.toString());
    }

    /**
     * Prints a strng indented and fils the remaining totalSpace with spaces
     *
     * @param string     the string
     * @param totalSpace the total available space (should be more than string length)
     * @param indent     the indent left of the string
     * @return the formatted string
     */
    public static String getStringLeftAligned(String string, int totalSpace, int indent)
    {
        if (totalSpace < string.length())
            string = string.substring(0, totalSpace);
        String str = "";
        for (int i = 0; i < indent; i++)
        {
            str += " ";
        }
        str += string;
        int spaceLeft = totalSpace - indent - str.length();
        for (int i = 0; i < spaceLeft; i++)
        {
            str += " ";
        }

        return str;
    }

    /**
     * Writes the string and fills the remaining space with empty spaces
     * Centers text
     *
     * @param text        the text to be centered
     * @param totalSpaces the total space available
     * @return the string with length = totalSpaces
     */
    public static String centerTextRelativeToSpace(String text, int totalSpaces)
    {
        int textToFill = totalSpaces - text.length();
        String line = "";
        for (int i = 0; i < textToFill / 2; i++)
        {
            line += " ";
        }
        line += text;
        for (int i = 0; i <= textToFill - textToFill / 2; i++)
        {
            line += " ";
        }

        return line;
    }


    public static void printInline(String indentBetweenCards, StringBuilder sb, String[][] cardsToString)
    {
        for (int i = 0; i < cardsToString[0].length; i++)
        {
            for (int j = 0; j < cardsToString.length; j++)
            {
                try
                {
                    sb.append(cardsToString[j][i] + ANSI.ANSI_RESET + indentBetweenCards);
                } catch (ArrayIndexOutOfBoundsException e)
                {

                }
            }
            sb.append("\n");
        }
    }
}
