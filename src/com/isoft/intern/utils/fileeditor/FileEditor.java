package com.isoft.intern.utils.fileeditor;

import java.io.File;

/**
 * User: Nikolay Uzunov
 */
public class FileEditor
{
    /**
     * This method rename a file.
     *
     * @param oldName holds new file name.
     * @param newName holds old file name.
     */
    public static boolean renameFile(String path, String oldName, String newName)
    {
        File file = new File(path + oldName + ".json");
        File newFile = new File(path + newName + ".json");
        if (file.renameTo(newFile))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean deleteFile(File file)
    {
        if (file.delete())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
