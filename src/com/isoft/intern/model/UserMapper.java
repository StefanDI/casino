package com.isoft.intern.model;

import org.json.JSONObject;
import sun.security.x509.AVA;

import static com.isoft.intern.model.UserAttributes.*;

/**
 * @author Nikolay Uzunov
 * This class serves to serialize User objects to JSONObject or deserialize JSONObject to User Object.
 */
public class UserMapper
{
    /**
     * Serialize User to JSONObject
     *
     * @param user User object for serialization.
     * @return returns JSONObject in string format
     */
    public static String serialize(User user)
    {
        JSONObject userJson = new JSONObject();

        userJson.put(FIRST_NAME.getValue(), user.getFirstName());
        userJson.put(MIDDLE_NAME.getValue(), user.getMiddleName());
        userJson.put(LAST_NAME.getValue(), user.getLastName());
        userJson.put(AGE.getValue(), String.valueOf(user.getAge()));
        userJson.put(ID_NUMBER.getValue(), user.getIdNumber());
        userJson.put(EMAIL.getValue(), user.getEmail());
        userJson.put(USERNAME.getValue(), user.getUsername());
        userJson.put(PASSWORD.getValue(), user.getPassword());
        userJson.put(IS_LOCKED.getValue(), String.valueOf(user.isLocked()));
        userJson.put(USER_ROLE.getValue(), user.getUserRole().toString());
        userJson.put(AVAILABLE_MONEY.getValue(), String.valueOf(user.getAvailableMoney()));

        return userJson.toString();
    }

    /**
     * Deserialize JSONObject to User
     *
     * @param userJson JSONObject for serialization.
     * @return returns User object.
     */
    public static User deserialize(JSONObject userJson)
    {
        User user = new User();
        user.setFirstName(userJson.getString(FIRST_NAME.getValue()));
        user.setMiddleName(userJson.getString(MIDDLE_NAME.getValue()));
        user.setLastName(userJson.getString(LAST_NAME.getValue()));
        user.setAge(Integer.valueOf(userJson.getString(AGE.getValue())));
        user.setIdNumber(userJson.getString(ID_NUMBER.getValue()));
        user.setEmail(userJson.getString(EMAIL.getValue()));
        user.setUsername(userJson.getString(USERNAME.getValue()));
        user.setPassword(userJson.getString(PASSWORD.getValue()));
        user.setLocked(Boolean.valueOf(userJson.getString(IS_LOCKED.getValue())));
        user.setAvailableMoney(Double.valueOf(userJson.getString(AVAILABLE_MONEY.getValue())));
        UserRoles userRole = UserRoles.valueOf(userJson.getString(USER_ROLE.getValue()));
        user.setUserRole(userRole);
        return user;
    }
}
