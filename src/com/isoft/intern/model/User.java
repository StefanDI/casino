package com.isoft.intern.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Nikolay Uzunov
 */
public class User implements Serializable
{
    public static final long serialVersionUID = -8588980448693010399L;

    private String firstName;
    private String middleName;
    private String lastName;
    private int age;
    private String idNumber;
    private String email;
    private String username;
    private String password;
    private boolean isLocked;
    private double availableMoney;

    private UserRoles userRole;

    public User()
    {
        this.userRole = UserRoles.PLAYER;
        this.availableMoney = 0;
    }

    public User(String firstName,
                String middleName,
                String lastName,
                int age,
                String idNumber,
                String email,
                String username,
                String password)
    {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.age = age;
        this.idNumber = idNumber;
        this.email = email;
        this.username = username;
        this.password = password;
        this.isLocked = false;
        this.userRole = UserRoles.PLAYER;
        this.availableMoney = 0;
    }

    public User(User user)
    {
        this.firstName = user.getFirstName();
        this.middleName = user.getMiddleName();
        this.lastName = user.getLastName();
        this.age = user.getAge();
        this.idNumber = user.getIdNumber();
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.isLocked = user.isLocked;
        this.userRole = user.getUserRole();
        this.availableMoney = user.getAvailableMoney();
    }


    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getMiddleName()
    {
        return middleName;
    }

    public void setMiddleName(String middleName)
    {
        this.middleName = middleName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getIdNumber()
    {
        return idNumber;
    }

    public void setIdNumber(String idNumber)
    {
        this.idNumber = idNumber;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isLocked()
    {
        return isLocked;
    }

    public void setLocked(boolean locked)
    {
        isLocked = locked;
    }

    public UserRoles getUserRole()
    {
        return userRole;
    }

    public void setUserRole(UserRoles userRole)
    {
        this.userRole = userRole;
    }

    public double getAvailableMoney()
    {
        return availableMoney;
    }

    public void setAvailableMoney(double availableMoney)
    {
        this.availableMoney = availableMoney;
    }

    @Override
    public String toString()
    {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", idNumber=" + idNumber +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", isLocked=" + isLocked +
                ", roles=" + userRole +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return email.equals(user.email) &&
                username.equals(user.username);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(email, username);
    }
}
