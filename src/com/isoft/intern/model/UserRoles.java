package com.isoft.intern.model;

/**
 * User: Nikolay Uzunov
 * Enumeration which holds all user's roles
 */
public enum UserRoles
{
    PLAYER, ADMIN
}
