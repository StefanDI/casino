package com.isoft.intern.model;

/**
 * @author Nikolay Uzunov
 * Enumeration which holds all user's attributes.
 */
public enum UserAttributes
{
    FIRST_NAME ("FIRST_NAME"),
    MIDDLE_NAME ("MIDDLE_NAME"),
    LAST_NAME  ("LAST_NAME"),
    AGE  ("AGE"),
    ID_NUMBER ("ID_NUMBER"),
    EMAIL ("EMAIL"),
    USERNAME ("USERNAME"),
    PASSWORD ("PASSWORD"),
    IS_LOCKED ("IS_LOCKED"),
    USER_ROLE ("USER_ROLE"),
    AVAILABLE_MONEY ("AVAILABLE_MONEY");

    UserAttributes(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    private String value;
}
