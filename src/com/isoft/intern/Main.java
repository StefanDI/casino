package com.isoft.intern;

import com.isoft.intern.exceptions.UserLockedException;
import com.isoft.intern.io.ConsoleInputReader;
import com.isoft.intern.io.ConsoleOutputWriter;
import com.isoft.intern.services.impl.UserServiceImpl;
import com.isoft.intern.utils.LocaleParser;
import com.isoft.intern.utils.validator.UtilValidator;
import com.isoft.intern.view.ActionHandler;
import com.isoft.intern.view.LocaleBuilder;
import com.isoft.intern.view.MenuTreeBuilder;
import com.isoft.intern.view.UserInputHandler;
import com.isoft.intern.view.exceptions.NodeNotFoundException;
import com.isoft.intern.view.menus.BlackjackMenu;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Stefan Ivanov
 */
public class Main
{


    private static final String MENU_ARCHITECTURE_RELATIVE_PATH = "database/menu_architecture.json";
    private static final String LOCALES_FOLDER_RELATIVE_PATH = "database/locales";
    private static final String DEFAULT_LOCALE_PATH = LOCALES_FOLDER_RELATIVE_PATH + "/locale_en.json";

    private static final String INITIAL_MENU = "main_menu";


    /**
     * Instantiate required objects for the UserInputHandler
     * And acceptSockets it
     *
     * @param args
     * @throws IOException           If menu hierarchy json file cannot be read (not found or IOException)
     * @throws NodeNotFoundException If {@link #INITIAL_MENU} is invalid tree node from the menu
     */
    public static void main(String[] args) throws IOException, NodeNotFoundException
    {
//        AnsiConsole.systemInstall();

        configureLogger();
        String chosenLocale = promptUserForLocale();

        ConsoleInputReader consoleInputReader = new ConsoleInputReader();
        LocaleBuilder localeBuilder = new LocaleBuilder(chosenLocale);

        MenuTreeBuilder menuTreeBuilder = new MenuTreeBuilder(MENU_ARCHITECTURE_RELATIVE_PATH);
        ActionHandler actionHandler = new ActionHandler();
        ConsoleOutputWriter consoleOutputWriter = new ConsoleOutputWriter();


        UserInputHandler userInputHandler = new UserInputHandler(consoleInputReader,
                consoleOutputWriter,
                localeBuilder,
                menuTreeBuilder,
                actionHandler,
                new UserServiceImpl(),
                INITIAL_MENU);
        userInputHandler.start();


//        try
//        {
//            new UserServiceImpl().login("stefan", "stefan");
//            BlackjackMenu menu = new BlackjackMenu();
//            menu.joinGame();
//        } catch (UserLockedException e)
//        {
//            e.printStackTrace();
//        }
    }


    /**
     * I know there is a lot wrong with this method
     * Should work this logic into another class
     */
    private static String promptUserForLocale()
    {
        Map<Integer, String> availableLocales = LocaleParser.getLocales(LOCALES_FOLDER_RELATIVE_PATH);
        for (Integer index : availableLocales.keySet())
        {
            System.out.println(index + ". " + availableLocales.get(index));
        }
        Scanner sc = new Scanner(System.in);
        String input;
        while (true)
        {
            input = sc.nextLine();
            if (UtilValidator.isNumber(input))
                break;
            System.out.println("Insert a number");
        }

        int inputAsInt = Integer.parseInt(input);

        //If invalid input is provided, return the default english locale
        if (availableLocales.containsKey(inputAsInt))
            return LOCALES_FOLDER_RELATIVE_PATH + "/locale_" + availableLocales.get(inputAsInt) + ".json";
        else
            return DEFAULT_LOCALE_PATH;
    }


    private static void configureLogger() throws IOException
    {
        Logger globalLogger = Logger.getGlobal();

        String timeOfCreation = new SimpleDateFormat("EEE-hh-mm").format(new Date());
        String logFilePath = String.format("log/%s_logs.txt", timeOfCreation);

        FileHandler handler = new FileHandler(logFilePath);

        globalLogger.addHandler(handler);
        SimpleFormatter formatter = new SimpleFormatter();
        handler.setFormatter(formatter);
        globalLogger.setUseParentHandlers(false);
    }

//    public static void printRoulette(String betOn)
//    {
//        System.out.println("------------");
//        System.out.println("|");
//    }


}

