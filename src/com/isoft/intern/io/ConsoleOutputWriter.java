package com.isoft.intern.io;

import com.isoft.intern.io.interfaces.OutputWriter;

/**
 * @author Stefan Ivanov
 */
public class ConsoleOutputWriter implements OutputWriter
{
    @Override
    public void write(String data)
    {
        System.out.print(data);
    }
}
