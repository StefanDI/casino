package com.isoft.intern.io.interfaces;

/**
 * @author Nikolay Uzunov
 * This is IO API for writing.
 */
public interface OutputWriter {

    void write(String data);
}
