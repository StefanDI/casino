package com.isoft.intern.io.interfaces;

/**
 * @author Stefan Ivanov
 * This is IO API for reading.
 */
public interface InputReader {

    String readLine();
}
