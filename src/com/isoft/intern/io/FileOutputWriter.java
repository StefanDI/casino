package com.isoft.intern.io;

import com.isoft.intern.io.interfaces.OutputWriter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;


/**
 * @author Nikolay Uzunov
 */
public class FileOutputWriter implements OutputWriter
{
    private static final Logger LOGGER = Logger.getLogger(FileOutputWriter.class.getName());
    private BufferedWriter fileWriter;

    /**
     * Instantiates FileOutputWriter by a given file name
     *
     * @param fileName the file name
     * @throws IOException if there is no such file
     */
    public FileOutputWriter(String fileName) throws IOException
    {
        fileWriter = new BufferedWriter(new FileWriter(fileName, true));
    }

    /**
     * Write data in file.
     *
     * @param data holds the data for writing in file.
     */
    @Override
    public void write(String data)
    {
        try
        {
            fileWriter.write(data);
            fileWriter.flush();
        } catch (FileNotFoundException e)
        {
            LOGGER.warning(e.getStackTrace() + " The file is not found!");
        } catch (IOException e)
        {
            LOGGER.warning(e.getStackTrace() + "Couldn't write in a file!");
        }
    }
}
