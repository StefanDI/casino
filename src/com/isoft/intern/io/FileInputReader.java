package com.isoft.intern.io;

import com.isoft.intern.io.interfaces.InputReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

/**
 * @author Stefan Ivanov
 */
public class FileInputReader implements InputReader
{

    private RandomAccessFile fileReader;

    /**
     * Instantiates FileInputReader by a given file name
     *
     * @param fileName the file name
     * @throws FileNotFoundException if there is no such file
     */
    public FileInputReader(String fileName) throws FileNotFoundException
    {
        fileReader = new RandomAccessFile(fileName, "r");
    }

    /**
     * Reads line and returns it proper encoded
     *
     * @return new UTF-8 encoded String
     */
    @Override
    public String readLine()
    {
        try
        {
            return new String(
                    fileReader.readLine()
                            .getBytes(StandardCharsets.ISO_8859_1),
                    StandardCharsets.UTF_8);
        } catch (IOException e)
        {
            return e.getMessage();
        }
    }

    /**
     * Reads all the lines of the given file
     *
     * @return UTF 8 encoded String representation of the bytes from the file
     * @throws IOException from {@link RandomAccessFile} readLine method
     */
    public String readAll() throws IOException
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                String line = fileReader.readLine();
                if (line == null || line.isEmpty())
                    break;
                sb.append(line);
            }
            return new String(sb.toString().getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        }
        finally
        {
            fileReader.close();
        }
    }
}
