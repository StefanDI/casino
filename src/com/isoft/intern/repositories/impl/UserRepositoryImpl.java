package com.isoft.intern.repositories.impl;

import com.isoft.intern.io.FileInputReader;
import com.isoft.intern.io.interfaces.OutputWriter;
import com.isoft.intern.model.User;
import com.isoft.intern.model.UserAttributes;
import com.isoft.intern.repositories.api.Repository;
import com.isoft.intern.repositories.api.UserRepository;
import com.isoft.intern.services.impl.UserServiceImpl;
import com.isoft.intern.userholder.LoggedInUser;
import com.isoft.intern.utils.fileeditor.FileEditor;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Nikolay Uzunov
 * Тhis class serves for communication with the database (files).
 */
public class UserRepositoryImpl implements UserRepository
{
    OutputWriter writer;
    FileInputReader reader;
    public static final String USER_JSON_PATH = "database/users/";
    public static final String LOGIN_DATA_PATH = "database/login_data.json";
    private static final Logger LOGGER = Logger.getGlobal();

    public UserRepositoryImpl(OutputWriter writer, FileInputReader reader)
    {
        this.writer = writer;
        this.reader = reader;
    }

    /**
     * This method insert object to file.
     *
     * @param user  holds the all information for user.
     */
    @Override
    public void insert(JSONObject user) throws IOException
    {
        //Create new file for user, which stored all data for it.
        String username = user.getString(UserAttributes.USERNAME.getValue());
        FileWriter userWriter = new FileWriter(USER_JSON_PATH + username + ".json");
        userWriter.write(user.toString());
        userWriter.flush();
        userWriter.close();
    }

    /**
     * Тhis method serves to update login data.
     * @param loginDataJsonString holds the updated login data.
     */
    public void updateLoginData(String loginDataJsonString)
    {
        clearFile();
        writer.write(loginDataJsonString);
    }

    /**
     * Тhis method serves to update user.
     * @param user holds the updated user.
     */
    @Override
    public void update(JSONObject user) throws IOException
    {
        //Create new file for user, which stored all data for it.
        String username =  user.getString(UserAttributes.USERNAME.getValue());
        String oldUsername = LoggedInUser.getInstance().getCurrentlyLoggedUser().getUsername();
        FileEditor.renameFile(UserRepositoryImpl.USER_JSON_PATH,oldUsername, username);
        FileWriter userWriter = new FileWriter(USER_JSON_PATH + username + ".json");
        userWriter.write(user.toString());
        userWriter.flush();
        userWriter.close();
    }

    /**
     * This method gets all data from login_data.json and clear it.
     */
    private void clearFile()
    {
        FileWriter clearFile;
        try
        {
            clearFile = new FileWriter(LOGIN_DATA_PATH);
            clearFile.close();
        }
        catch (IOException e)
        {
            LOGGER.log(Level.WARNING," Couldn't write in a file!", e);
        }
    }

    /**
     * This method gets all data from json.file.
     */
    public JSONObject getAllLoginData()
    {
        try
        {
            reader = new FileInputReader(LOGIN_DATA_PATH);
            String loginDataJsonString = reader.readAll();
            JSONObject loginDataJson = new JSONObject(loginDataJsonString);
            return loginDataJson;
        }
        catch (IOException e)
        {
            LOGGER.log(Level.WARNING,"Couldn't read a file!", e);
        }

        return null;
    }

    /**
     * This method get user by his username.
     *
     * @param username holds User's username.
     */
    @Override
    public JSONObject getUserByUsername(String username)
    {
        try
        {
            FileInputReader fileInputReader = new FileInputReader(USER_JSON_PATH + username + ".json");
            String userJsonString = fileInputReader.readAll();
            JSONObject userJson = new JSONObject(userJsonString);
            return userJson;
        }
        catch (FileNotFoundException e)
        {
            LOGGER.log(Level.WARNING, " File not found!", e);
        }
        catch (IOException e)
        {
            LOGGER.log(Level.WARNING, " Couldn't read a file!", e);
        }
        return null;
    }

    /**
     * Тhis method serves to get all users.
     */
    @Override
    public List<JSONObject> getAll()
    {
        List<JSONObject> jsonUsers = new ArrayList<>();
        JSONObject usernamesJson = (JSONObject) getAllLoginData().get(UserServiceImpl.USERNAMES);

        for (String key : usernamesJson.keySet())
        {
            try
            {
                String fileName = USER_JSON_PATH + key + ".json";
                FileInputReader fileInputReader = new FileInputReader(fileName);
                String userJsonString = fileInputReader.readAll();
                JSONObject userJson = new JSONObject(userJsonString);
                jsonUsers.add(userJson);
            }
            catch (FileNotFoundException e)
            {
                LOGGER.log(Level.WARNING, "User file is not found! " + key, e);
            }
            catch (IOException e)
            {
                LOGGER.log(Level.WARNING,  " Couldn't read a file!", e);
            }
        }
        return jsonUsers;
    }

    /**
     * This method serves to delete a user.
     *
     * @param user holds the user with will be deleted.
     */
    @Override
    public void delete(JSONObject user)
    {
        String username = user.getString(UserAttributes.USERNAME.getValue());
        if (FileEditor.deleteFile(new File(UserRepositoryImpl.USER_JSON_PATH + username + ".json")))
        {
            LOGGER.log(Level.INFO, "User: " + username + "is deleted!");
        }
    }
}
