package com.isoft.intern.repositories.api;
import java.io.IOException;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public interface Repository<T>
{
    void insert(T userJson) throws IOException;
    void update(T userJson) throws IOException;
    void delete(T user);
    List<T> getAll();
}
