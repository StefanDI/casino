package com.isoft.intern.repositories.api;

import org.json.JSONObject;

/**
 * @author Nikolay Uzunov
 */
public interface UserRepository extends Repository<JSONObject>
{
    JSONObject getUserByUsername(String username);
}
