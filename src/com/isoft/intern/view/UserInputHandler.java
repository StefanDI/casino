package com.isoft.intern.view;

import com.isoft.intern.casino.Casino;
import com.isoft.intern.io.interfaces.InputReader;
import com.isoft.intern.io.interfaces.OutputWriter;
import com.isoft.intern.model.User;
import com.isoft.intern.services.api.UserService;
import com.isoft.intern.view.exceptions.IllegalActionException;
import com.isoft.intern.view.exceptions.NodeNotFoundException;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class holds ll the useful objects and methods for all classes in menus folder
 * a.k.a all the classes that connects user input and business logic
 * List of all the static members, and what they are used for:
 *
 * <b>Objects:</b>
 * {@link LocaleBuilder} for accessing the current locale strings
 * {@link InputReader}  for reading input
 * {@link OutputWriter} for writing output
 * {@link MenuTreeBuilder} for navigating to nodes and building menuMap
 * {@link ActionHandler} for handling actionNodes
 * {@link Casino} for accessing bets withing child classes (WILL BE REWORKED)
 * <b>Methods</b>
 * {@link #getString(String)} returns the translation of the given String, from the chosen language
 * {@link #redirectTo(String)} redirects the user to the node with the chosen name as parameter
 * {@link #redirectTo(String, boolean)} redirects the user to the node and saves the last node as history (for back option)
 * <p>
 *
 *
 * @author Stefan Ivanov
 */

public final class UserInputHandler
{
    /*
        When this input is entered, go back
     */
    public static final String QUIT_ANY_TIME_OPTION = "Q";

    private final static Logger LOGGER = Logger.getGlobal();

    public static Casino casino;
    public static UserService userService;
    public static InputReader reader;
    public static OutputWriter writer;

    private static Map<Integer, MenuNode> currentMenuOptions;
    private static MenuNode currentNode;
    private static LocaleBuilder localeBuilder;
    private static MenuTreeBuilder menuTreeBuilder;
    private static ActionHandler actionHandler;

    private static boolean isRunning;

    /**
     * @param reader the console reader
     * @param writer the console writer
     * @param localeBuilder the locale builder
     * @param menuTreeBuilder the menu tree builder
     * @param actionHandler the action handler
     * @param userService the user service
     * @param initialMenuName string of the name of the menu from which we start
     * @throws NodeNotFoundException If the initialMenuName is non existent
     */
    public UserInputHandler(InputReader reader,
                            OutputWriter writer,
                            LocaleBuilder localeBuilder,
                            MenuTreeBuilder menuTreeBuilder,
                            ActionHandler actionHandler,
                            UserService userService,
                            String initialMenuName) throws NodeNotFoundException
    {

        UserInputHandler.reader = reader;
        UserInputHandler.writer = writer;
        UserInputHandler.localeBuilder = localeBuilder;
        UserInputHandler.menuTreeBuilder = menuTreeBuilder;
        UserInputHandler.actionHandler = actionHandler;
        UserInputHandler.userService = userService;
        currentNode = menuTreeBuilder.getNodeByName(initialMenuName);

        casino = new Casino();

    }

    public void start()
    {
        isRunning = true;
        while (isRunning)
        {
            run();
        }
    }

    /**
     * Returns the translation to the current for a chosen key
     *
     * @param key the key
     * @return the translation of the key or the key if there is no translation
     */
    public static String getString(String key)
    {
        return localeBuilder.getString(key);
    }

    /**
     * Stops the 'app loop'
     * Exit
     */
    public static void stopProgram()
    {
        isRunning = false;
        writer.write("-----------BYE BYE-----------");
    }

    /**
     * Redirects the user to the MenuNode with the chosen name
     * Stays the same if the node is not found
     *
     * @param menuNodeName the name of the menu node
     */
    public static void redirectTo(String menuNodeName)
    {
        redirectTo(menuNodeName, false);
    }

    /**
     * Redirects the user to the MenuNode with the chosen name,
     * and adds the last node to the history
     * Stays the same if the node is not found
     *
     * @param menuNodeName the name of the menu node
     */
    public static void redirectTo(String menuNodeName, boolean addToHistory)
    {
        try
        {
            MenuNode lastNode = currentNode;
            currentNode = menuTreeBuilder.getNodeByName(menuNodeName);

            if (addToHistory)
                currentNode.redirectedFrom = lastNode;

        } catch (NodeNotFoundException e)
        {
            //No such node abort redirect
            //current node remains the same
            LOGGER.warning("Missing node: " + menuNodeName + " Menu node stays the same: " + currentNode.content);
        }
    }

    /**
     * The "Menu loop"
     * If we exit this method, its game over :)
     */
    public void run()
    {
        if (!isRunning)
            return;
        currentMenuOptions = menuTreeBuilder.buildMenuMap(currentNode);

        if (!currentNode.children.isEmpty() &&
                currentNode.children.get(0).isLeaf)
        {
            try
            {
                MenuNode n = currentNode.children.get(0);
                //If something happends and we dont get redirected, we get returned to the parent view
                currentNode = currentNode.parent;
                actionHandler.invokeAction(n, getRoot(currentNode));
                return;
            } catch (IllegalActionException e)
            {
                e.printStackTrace();
                LOGGER.log(Level.SEVERE, "Could not invokeAction() \n" + e);
            }
        }
        int i = 1;
        for (Integer optionIndex : currentMenuOptions.keySet())
        {
            MenuNode option = currentMenuOptions.get(optionIndex);

            String optionForCurrentLocale = localeBuilder.getString(option.content);
            writer.write(i++ + ". " + optionForCurrentLocale + "\n");
        }

        if (currentNode.parent != null || currentNode.redirectedFrom != null)
        {
            int backOption = currentMenuOptions.size() + 1;
            writer.write(backOption + ". " + localeBuilder.getString("back") + "\n");

            MenuNode node = currentNode.parent != null ? currentNode.parent : currentNode.redirectedFrom;
            currentMenuOptions.put(backOption, node);
        }

        int userInput = getValidUserInput();

        currentNode = currentMenuOptions.get(userInput);

    }

    private MenuNode getRoot(MenuNode actionNode)
    {
        while (actionNode.parent != null)
            actionNode = actionNode.parent;

        return actionNode;
    }

    private boolean isValid(String userInput)
    {
        if (userInput == null)
            return false;
        try
        {
            int num = Integer.parseInt(userInput);
            if (num <= 0 || num > currentMenuOptions.size())
                return false;
        } catch (NumberFormatException e)
        {
            return false;
        }

        return true;
    }

    private int getValidUserInput()
    {
        String userInput;
        while (true)
        {
            userInput = reader.readLine();
            if (isValid(userInput))
                break;
            writer.write(localeBuilder.getString("invalid_input")+"\n");
        }
        return Integer.parseInt(userInput);
    }
}
