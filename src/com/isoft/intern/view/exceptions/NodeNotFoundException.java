package com.isoft.intern.view.exceptions;

import com.isoft.intern.view.MenuNode;

/**
 * Exception is thrown when there is no such {@link MenuNode} in the Menu tree
 * Its only thrown in {@link com.isoft.intern.view.MenuTreeBuilder#getNodeByName(String)}
 * @author Stefan Ivanov
 */
public class NodeNotFoundException extends Exception
{

    public NodeNotFoundException(String message)
    {
        super(message);
    }
}
