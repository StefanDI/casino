package com.isoft.intern.view.exceptions;

import com.isoft.intern.view.ActionHandler;
import com.isoft.intern.view.MenuNode;

/**
 * Exception is thrown when invalid action is passed to the {@link ActionHandler} class
 * {@see ActionHandler} documentation for valid action's
 * exception is only thrown in {@link ActionHandler#invokeAction(MenuNode, MenuNode)}
 *
 * @author Stefan Ivanov
 */
public class IllegalActionException extends Exception
{
    public IllegalActionException(String s, Throwable cause)
    {
        super(s, cause);
    }

}
