package com.isoft.intern.view;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class builds a Map from the file path to a valid json file with locale
 * key is the {@link MenuNode#content} : value is the translation to the locale's language
 *
 * @author Stefan Ivanov
 */
public class LocaleBuilder
{
    private final Logger LOGGER = Logger.getGlobal();
    private static JSONObject localesString;

    /**
     * @param filePath the path to the file with the JSON represented locale key value strings
     */
    public LocaleBuilder(String filePath)
    {
        buildLocale(filePath);
    }

    /**
     * If there is no such element, return the key
     *
     * @param key the key of the locale string
     * @return the value for the given string
     */
    String getString(String key)
    {
        if (localesString == null)
            throw new IllegalArgumentException("Locales not loaded, load locales before accessing strings\n");

        //If there is such value - return it
        //catch -> no such value, return the key
        try
        {
            return localesString.getString(key);
        } catch (JSONException noSuchValueInLocale)
        {
            return key;
        }

    }

    private void buildLocale(String filePath)
    {
        try (RandomAccessFile fileReader = new RandomAccessFile(filePath, "r"))
        {
            String line;
            StringBuilder data = new StringBuilder();
            while ((line = fileReader.readLine()) != null)
            {
                //For the proper UTF encoding
                data.append(new String(line.getBytes(StandardCharsets.ISO_8859_1),
                        StandardCharsets.UTF_8));
            }
            localesString = new JSONObject(data.toString());
        } catch (FileNotFoundException e)
        {
            LOGGER.log(Level.WARNING, "File not found: " + filePath
                    + "\nLocales failed to load\n", e);
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING, "Locales failed to load\n", e);
        }
    }
}
