package com.isoft.intern.view;

import com.isoft.intern.view.exceptions.IllegalActionException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This class is responsible for invoking a method by a given MenuNode(actionNode) and MenuNode(root of the actionNode)
 * there must be a class named the same as the root MenuNode (if root is user_menu, class should be UserMenu)
 * the class must have public constructor with 0 arguments or default constructor
 * that class must have a public method with 0 arguments with name the same as actionNode name
 *
 * @author Stefan Ivanov
 */
public class ActionHandler
{
    public ActionHandler()
    {
    }

    /**
     * Invokes method from corresponding to the node's name (content)
     * The method should be located in a class, named as the node's root
     *
     * @param actionNode the leaf node with the method name as content
     * @param root       the root node of the actionNode, needed to extract the class name
     * @throws IllegalActionException If there is no such method, class, or method cannot be invoked
     */
    public void invokeAction(MenuNode actionNode, MenuNode root) throws IllegalActionException
    {

        try
        {
            Class clazz = extractClass(root);
            Method m = extractMethod(actionNode, clazz);
            m.invoke(clazz.newInstance());
        } catch (InvocationTargetException e)
        {
            throw new IllegalActionException("Could not invoke method", e);
        } catch (NoSuchMethodException e)
        {
            throw new IllegalActionException("There is no such method " + actionNode.content, e);
        } catch (IllegalAccessException e)
        {
            throw new IllegalActionException("Method cannot be accessed, maybe declared private? " + actionNode.content, e);
        } catch (ClassNotFoundException e)
        {
            throw new IllegalActionException("There is no such class " + root.content, e);
        } catch (InstantiationException e)
        {
            throw new IllegalActionException("Could not instantiate class " + root.content, e);
        }
    }

    private Method extractMethod(MenuNode actionNode, Class clazz) throws NoSuchMethodException
    {
        for (Method method : clazz.getDeclaredMethods())
        {
            if (method.getName().toLowerCase().equals(
                    actionNode.content.trim().toLowerCase()))
                return method;
        }
        throw new NoSuchMethodException();
    }

    private Class extractClass(MenuNode root) throws ClassNotFoundException
    {
        String[] classNames = root.content.split("_");
        StringBuilder correctClassName = new StringBuilder();
        for (String className : classNames)
        {
            correctClassName.append(
                    className.substring(0, 1)
                            .toUpperCase())
                    .append(className.substring(1));
        }

        //All menu files should be in this package with subpackage .menus
        String packageName = this.getClass().getPackage().getName() + ".menus";
        return Class.forName(packageName + "." + correctClassName.toString());
    }

}
