package com.isoft.intern.view.menus;

import com.isoft.intern.model.User;
import com.isoft.intern.passwordencrypter.PasswordEncrypter;
import com.isoft.intern.userholder.LoggedInUser;
import com.isoft.intern.utils.validator.UserValidator;
import com.isoft.intern.utils.validator.UtilValidator;

import static com.isoft.intern.view.UserInputHandler.*;

/**
 * @author Petar Hristov
 */
public final class UserMenu
{
    private static final String MAIN_MENU = "main_menu";
    private static final String LOGIN_MENU = "login";
    private static final String EDIT_PROFILE = "edit_profile";

    public void adminMenu()
    {
        redirectTo("admin_menu", true);
    }

    public void logout()
    {
        if (LoggedInUser.getInstance().isLoggedIn())
            LoggedInUser.getInstance().logoutCurrentUser();

        redirectTo(MAIN_MENU, false);
    }

    public void joinRoulette()
    {
        writer.write(getString("welcome_to_roulette") + "\n");
        redirectTo("roulette_menu", true);
    }

    public void joinBlackjack()
    {
        writer.write(getString("welcome_to_blackjack") + "\n");
        redirectTo("blackjack_menu", true);
    }

    /**
     * method used for depositing money into the logged in user's account
     */
    public void deposit()
    {
        if (isNotLoggedIn()) return;
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        double moneyDep = loggedInUser.getAvailableMoney();
        writer.write(getString("available_money") + moneyDep + "\n");

        String depositAmount;
        while (true)
        {
            writer.write("\n" + getString("insert_deposit"));
            depositAmount = reader.readLine();
            if (!UtilValidator.isDoubleMoney(depositAmount))
                writer.write(getString("deposit_error") + "\n");
            else
                break;
        }
        writer.write(getString("deposit_succ") + "\n");

        Double newTotalMoney = moneyDep + Double.parseDouble(depositAmount);
        loggedInUser.setAvailableMoney(newTotalMoney);
        userService.updateUser(loggedInUser);
    }

    /**
     * method used for withdrawing money from the logged in user's account
     */
    public void withdraw()
    {
        if (isNotLoggedIn()) return;
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();

        double moneyWith = loggedInUser.getAvailableMoney();
        writer.write(getString("available_money") + moneyWith + "\n");

        String withdrawMoney;
        while (true)
        {
            writer.write("\n" + getString("insert_withdraw"));
            withdrawMoney = reader.readLine();
            if (!UtilValidator.isDoubleMoney(withdrawMoney))
                writer.write(getString("withdraw_error") + "\n");
            else if (Double.parseDouble(withdrawMoney) > moneyWith)
                writer.write(getString("withdraw_error_no_money") + "\n");
            else
                break;
        }
        writer.write(getString("withdraw_succ") + "\n");

        Double newTotalMoney = moneyWith - Double.parseDouble(withdrawMoney);
        loggedInUser.setAvailableMoney(newTotalMoney);
        userService.updateUser(loggedInUser);

    }


    public void editUsername()
    {
        if (isNotLoggedIn()) return;
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        writer.write(getString("current_username") + " " + loggedInUser.getUsername() + "\n");

        String newUsername;
        while (true)
        {
            writer.write(getString("enter_new_username"));
            newUsername = reader.readLine();

            if (UserValidator.isValidSize(newUsername) && UserValidator.containsValidSymbols(newUsername))
            {
                User user = new User(LoggedInUser.getInstance().getCurrentlyLoggedUser());
                user.setUsername(newUsername);
                userService.updateUser(user);
                break;
            }
            writer.write(getString("username_error") + " or " + getString("username_forbbiden_symbol") + "\n");
        }
        redirectTo(EDIT_PROFILE);
    }

    public void editPassword()
    {
        if (isNotLoggedIn()) return;
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        String newPassword;
        while (true)
        {
            writer.write(getString("enter_new_password"));
            newPassword = reader.readLine();

            if (UserValidator.passwordIsValidSize(newPassword))
            {
                loggedInUser.setPassword(PasswordEncrypter.encryptPassword(newPassword));
                userService.updateUser(loggedInUser);
                break;
            } else
                writer.write(getString("password_error") + "\n");
        }
        redirectTo(EDIT_PROFILE);
    }

    public void editAge()
    {
        if (isNotLoggedIn()) return;
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();

        writer.write(getString("current_age") + " " + loggedInUser.getAge() + " \n");

        String newAge;
        while (true)
        {
            writer.write(getString("enter_new_age"));
            newAge = reader.readLine();
            if (UtilValidator.isNumber(newAge) && UserValidator.isLegalAge(newAge))
            {
                loggedInUser.setAge(Integer.parseInt(newAge));
                userService.updateUser(loggedInUser);
                break;
            } else
                writer.write(getString("age_legal_error") + getString("age_number_error") + "\n");
        }
        redirectTo(EDIT_PROFILE);
    }

    public void editFirstName()
    {
        if (isNotLoggedIn())
        {
            return;
        }
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        writer.write(getString("current_first_name") + " " + loggedInUser.getFirstName() + "\n");

        String newFirstName;
        while (true)
        {
            writer.write(getString("enter_new_first_name"));
            newFirstName = reader.readLine();
            if (UtilValidator.nameContainsValidCharacters(newFirstName))
            {
                loggedInUser.setFirstName(newFirstName);
                userService.updateUser(loggedInUser);
                break;
            } else
                writer.write(getString("names_letters_error") + "\n");
        }
    }

    public void editMiddleName()
    {
        if (isNotLoggedIn())
        {
            return;
        }
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        writer.write(getString("current_middle_name") + " " + loggedInUser.getMiddleName() + "\n");

        String newMiddleName;
        while (true)
        {
            writer.write(getString("enter_new_middle_name"));
            newMiddleName = reader.readLine();
            if (UtilValidator.nameContainsValidCharacters(newMiddleName))
            {
                loggedInUser.setMiddleName(newMiddleName);
                userService.updateUser(loggedInUser);
                break;
            } else
                writer.write(getString("names_letters_error") + "\n");
        }
    }

    public void editLastName()
    {
        if (isNotLoggedIn())
        {
            return;
        }
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        writer.write(getString("current_last_name") + " " + loggedInUser.getLastName() + "\n");

        String newLastName;
        while (true)
        {
            writer.write(getString("enter_new_last_name"));
            newLastName = reader.readLine();
            if (UtilValidator.nameContainsValidCharacters(newLastName))
            {
                loggedInUser.setLastName(newLastName);
                userService.updateUser(loggedInUser);
                break;
            } else
                writer.write(getString("names_letters_error") + "\n");
        }
    }

    public void editEmail()
    {
        if (isNotLoggedIn())
        {
            return;
        }
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        writer.write(getString("current_email") + " " + loggedInUser.getEmail() + "\n");

        String newEmail;
        while (true)
        {
            writer.write(getString("enter_new_email"));
            newEmail = reader.readLine();
            if (UserValidator.isValidEmail(newEmail))
            {
                User user = new User(LoggedInUser.getInstance().getCurrentlyLoggedUser());
                user.setEmail(newEmail);
                userService.updateUser(user);
                break;
            } else
                writer.write(getString("email_error") + "\n");
        }
    }

    public void editIdNumber()
    {
        if (isNotLoggedIn())
        {
            return;
        }
        User loggedInUser = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        writer.write(getString("current_id_number") + " " + loggedInUser.getIdNumber() + "\n");

        String newIdNumber;
        while (true)
        {
            writer.write(getString("enter_new_id_number"));
            newIdNumber = reader.readLine();
            if (UtilValidator.isNumber(newIdNumber))
            {
                loggedInUser.setIdNumber(newIdNumber);
                userService.updateUser(loggedInUser);
                break;
            } else
                writer.write(getString("id_number_error") + "\n");
        }

    }

    /**
     * Reversed your method so i dont refactor everywhere
     */
    private boolean isNotLoggedIn()
    {
        if (LoggedInUser.getInstance().isLoggedIn())
        {
            return false;
        }
        writer.write(getString("not_logged_in"));
        redirectTo(LOGIN_MENU);
        return true;
    }
}
