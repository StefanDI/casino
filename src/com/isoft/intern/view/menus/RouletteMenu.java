package com.isoft.intern.view.menus;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.bets.BetHolder;
import com.isoft.intern.casino.games.roulette.bets.RouletteBet;
import com.isoft.intern.casino.games.roulette.rouletteentities.Player;
import com.isoft.intern.casino.games.roulette.visualizer.Visualizer;
import com.isoft.intern.userholder.LoggedInUser;
import com.isoft.intern.utils.validator.UtilValidator;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.isoft.intern.view.UserInputHandler.*;

/**
 * @author Nikolay Uzunov
 */
public class RouletteMenu
{
    private static final Logger LOGGER = Logger.getGlobal();
    private Roulette currentRoulette;
    private Thread mainThread = Thread.currentThread();

    public void startRoulette()
    {
        for (Roulette roulette : casino.getRouletteArrayList())
        {
            currentRoulette = roulette;
            Player player = new Player(LoggedInUser.getInstance().getCurrentlyLoggedUser());
            player.setMenu(this);
            currentRoulette.join(player);
        }
        try
        {
            Thread.sleep(1000000);
        }
        catch (InterruptedException e)
        {
            redirectTo("user_menu");
            //exit
        }
    }

    /**
     * Displays betting menu.
     *
     * @param visualizer holds Visualizer instance.
     */
    public void visualizeBetMenu(Visualizer visualizer)
    {
        betMenuLabel : while (true)
        {
            String[][] menu = new String[5][1];
            menu[0][0] = "Write a number or choose an option: ";
            menu[1][0] = "A) First dozen           B) Second dozen        C) Third dozen";
            menu[2][0] = "D) EVEN        G) ODD ";
            menu[3][0] = "E) RED         F) BLACK ";
            menu[4][0] = "H) LOW         I) HIGH";
            visualizer.printRouletteAnd(menu);
            writer.write("Write START when ready\n");
            String in = reader.readLine();

            if (in.toUpperCase().equals("START"))
            {
                if(!BetHolder.hasBets())
                {
                    writer.write(getString("have_to_bet") + "\n");
                }
                else
                {
                    break;
                }
            }
            switch (in.toUpperCase())
            {
                case "A":
                    if (!setBet(RouletteBet.FIRST_DOZEN))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "B":
                    if (!setBet(RouletteBet.SECOND_DOZEN))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "C":
                    if (!setBet(RouletteBet.THIRD_DOZEN))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "D":
                    if (!setBet(RouletteBet.EVEN))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "E":
                    if (!setBet(RouletteBet.RED))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "F":
                    if (!setBet(RouletteBet.BLACK))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "G":
                    if (!setBet(RouletteBet.ODD))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "H":
                    if (!setBet(RouletteBet.LOW))
                    {
                        break betMenuLabel;
                    }
                    break;
                case "I":
                    if (!setBet(RouletteBet.HIGH))
                    {
                        break betMenuLabel;
                    }
                    break;
                default:
                {
                    if (UtilValidator.isNumber(in))
                    {
                        int num = Integer.parseInt(in);
                        if (num >= 0 && num < 37)
                        {
                            if (!setBet(num))
                            {
                                break betMenuLabel;
                            }
                        }
                        else
                        {
                            writer.write(getString("number_between_error") + "\n");
                        }
                    }
                    else if (!in.toUpperCase().equals("START"))
                    {
                        writer.write(getString("invalid_input") + "\n");
                    }
                }
            }
        }
    }

    /**
     * Check if the map with winning bets is/is not empty displays corresponding message.
     * @param winningBets map with player's winning bet.
     */
    public void printWinningBets(Map<RouletteBet, Double> winningBets)
    {
        if (winningBets.isEmpty() && BetHolder.betSuccess)
        {
            writer.write("You loose your money!\n");
        }
        else
        {
            for (RouletteBet rouletteBetReward : winningBets.keySet())
            {
                writer.write("You won " +
                        winningBets.get(rouletteBetReward) +
                        " from " +
                        rouletteBetReward.toString() + "\n");
            }
        }
    }

    /**
     * Add new bet in the map with bets in {@link BetHolder}
     * @param rouletteBet holds the bet that player wants to bet on.
     * @return true or false if player's money are lower than his bet.
     */
    private boolean setBet(RouletteBet rouletteBet)
    {
        if (!LoggedInUser.getInstance().isLoggedIn())
        {
            writer.write(getString("not_logged_in"));
            redirectTo("login");
        }
        else
        {
            writer.write(getString("enter_bet") + " \n");
            String input = reader.readLine();
            if (UtilValidator.isDoubleMoney(input))
            {
                double bet = Double.parseDouble(input);
                if (LoggedInUser.getInstance().getCurrentlyLoggedUser().getAvailableMoney() >= bet)
                {
                    BetHolder.setBet(rouletteBet, bet);
                    BetHolder.betSuccess = true;
                    return true;
                }
                else
                {
                    writer.write(getString("bet_money_error") + "\n");
                    if (!BetHolder.hasBets())
                    {
                        BetHolder.betSuccess = false;
                    }

                }
            }
            else
            {
                writer.write(getString("not_a_number") + "\n");
                visualizeBetMenu(new Visualizer());
            }
        }
        return false;
    }

    /**
     * Add new number bet in the map with bets in {@link BetHolder}
     * @param number holds the number that player wants to bet on.
     * @return true or false if player's money are lower than his bet.
     */
    private boolean setBet(int number)
    {
        if (!LoggedInUser.getInstance().isLoggedIn())
        {
            writer.write(getString("not_logged_in"));
            redirectTo("login");
        }
        String inputBet;
        writer.write(getString("wish_bet"));
        inputBet = reader.readLine();
        if (UtilValidator.isDoubleMoney(inputBet))
        {
            double bet = Double.parseDouble(inputBet);
            if (LoggedInUser.getInstance().getCurrentlyLoggedUser().getAvailableMoney() >= bet)
            {
                bet = Double.parseDouble(inputBet);
                BetHolder.setBet(number, bet);
                if(!BetHolder.hasBets())
                {
                    BetHolder.betSuccess = true;
                }
                return true;
            }
            else
            {
                writer.write(getString("bet_money_error") + "\n");
                BetHolder.betSuccess = false;
            }

        }
        else
        {
            writer.write(getString("not_a_number") + "\n");
            visualizeBetMenu(new Visualizer());
        }
        return false;
    }

    /**
     * Remove the logged in player from roulette game.
     */
    public void playerExit()
    {
        currentRoulette.removePlayer();
        mainThread.interrupt();
    }

    /**
     * Displays exit menu.
     * @return if the player wants to exit returns true otherwise returns false.
     */
    public boolean hasQuit()
    {
        System.out.println("Choose an option: ");
        System.out.println("1). Exit.");
        System.out.println("2). Continue.");
        String input;
            input = reader.readLine();

            switch (input)
            {
                case "1":
                {
                    return true;
                }
                case "2":
                {
                    return false;
                }
                default:
                {
                    writer.write(getString("invalid_input") + "\n");
                }
            }
        return false;
    }
}
