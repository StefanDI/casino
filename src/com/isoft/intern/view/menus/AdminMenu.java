package com.isoft.intern.view.menus;

import com.isoft.intern.model.User;
import com.isoft.intern.utils.validator.UtilValidator;

import java.util.List;

import static com.isoft.intern.view.UserInputHandler.*;


/**
 * @author Stefan Ivanov
 */
public final class AdminMenu
{
    /**
     * This method serves to display all users with option to unlock and lock the account
     */
    public void listAllUsers()
    {
        List<User> allUsers = userService.getAllUsers();
        for (int i = 1; i <= allUsers.size(); i++)
        {
            writer.write(i + ". " + allUsers.get(i - 1).getUsername() + "\n");
        }

        String input;
        User chosenUser;
        writer.write(getString("choose_an_user"));
        while (true)
        {
            input = reader.readLine();
            if (UtilValidator.isNumber(input) && Integer.parseInt(input) < allUsers.size() + 1)
            {
                chosenUser = allUsers.get(Integer.valueOf(input) - 1);
                writer.write(chosenUser.toString() + "\n");
                writer.write("1. " + (chosenUser.isLocked() ? "unlock" : "lock") + "\n");
                writer.write("2. Back\n");
                input = reader.readLine();
                break;
            }
            else
            {
                writer.write("invalid_input");
            }
        }
        changeAccountStatus(input, chosenUser);
    }
    /**
     *  This method serves to change account status (locked or unlocked).
     * @param input input chosen from the admin.
     * @param chosenUser holds the user which will change the account status.
     */
    private void changeAccountStatus(String input, User chosenUser)
    {
        if(input.equals("1"))
        {
            boolean shouldBeLocked = !chosenUser.isLocked();
            chosenUser.setLocked(shouldBeLocked);
            userService.updateUser(chosenUser);
        }
        else if(input.equals("2"))
        {
            redirectTo("admin_menu");
        }
        else
        {
            writer.write(getString("invalid_input"));
        }
    }
}