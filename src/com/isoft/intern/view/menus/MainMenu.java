package com.isoft.intern.view.menus;

import com.isoft.intern.exceptions.UserLockedException;
import com.isoft.intern.model.User;
import com.isoft.intern.utils.validator.UserValidator;
import com.isoft.intern.utils.validator.UtilValidator;

import java.util.logging.Logger;

import static com.isoft.intern.view.UserInputHandler.*;

/**
 * @author Stefan Ivanov
 */
@SuppressWarnings("unused")
public final class MainMenu
{
    private static final Logger LOGGER = Logger.getLogger(MainMenu.class.getSimpleName());

    private static final String USER_MENU_NODE = "user_menu";
    private static final String LOGIN_MENU_NODE = "login";

    public void login()
    {
        writer.write(getString("welcome_to_login") + "\n");

        writer.write(getString("insert_username"));
        String username = reader.readLine();
        if (username.equals(QUIT_ANY_TIME_OPTION))
            return;
        writer.write(getString("password_key"));
        String password = reader.readLine();
        if (password.equals(QUIT_ANY_TIME_OPTION))
            return;

        try
        {
            if (userService.login(username, password))
            {
                writer.write(getString("login_success") + "\n");
                redirectTo(USER_MENU_NODE);
            } else
            {
                writer.write(getString("login_error") + "\n");
                redirectTo(LOGIN_MENU_NODE);
            }
        } catch (UserLockedException e)
        {
            writer.write(getString("account_locked") + "\n");
            redirectTo(LOGIN_MENU_NODE);
        }
    }

    public void register()
    {
        writer.write(getString("welcome_to_register") + "\n");

        String username;

        while (true)
        {
            writer.write(getString("insert_username"));
            username = reader.readLine();

            if (username.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (!UserValidator.isValidSize(username))
                writer.write(getString("username_error") + "\n");
            else if (!UserValidator.containsValidSymbols(username))
                writer.write(getString("username_forbbiden_symbol") + "\n");
            else if (userService.usernameExists(username))
                writer.write(getString("username_exists") + "\n");
            else
                break;
        }


        String password;
        while (true)
        {
            writer.write(getString("insert_password"));
            password = reader.readLine();

            if (password.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (!UserValidator.passwordIsValidSize(password))
                writer.write(getString("password_error") + "\n");
            else
                break;
        }


        String confirmPassword;
        while (true)
        {
            writer.write(getString("confirm_password") + "\n");
            confirmPassword = reader.readLine();

            if (confirmPassword.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (!confirmPassword.equals(password))
                writer.write(getString("confirm_pass_error") + "\n");
            else
                break;
        }


        String email;
        while (true)
        {
            writer.write(getString("insert_email"));
            email = reader.readLine();

            if (email.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (!UserValidator.isValidEmail(email))
                writer.write(getString("email_error") + "\n");
            else
                break;
        }

        String age;
        while (true)
        {
            writer.write(getString("insert_age"));
            age = reader.readLine();

            if (age.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (!UtilValidator.isNumber(age))
                writer.write(getString("age_number_error") + "\n");
            else if (!UserValidator.isLegalAge(age))
                writer.write(getString("age_legal_error") + "\n");
            else
                break;
        }

        String firstName;
        String middleName;
        String lastName;
        while (true)
        {
            writer.write(getString("insert_first_name"));
            firstName = reader.readLine();

            if (firstName.equals(QUIT_ANY_TIME_OPTION))
                return;

            writer.write(getString("insert_middle_name"));
            middleName = reader.readLine();

            if (middleName.equals(QUIT_ANY_TIME_OPTION))
                return;

            writer.write(getString("insert_last_name"));
            lastName = reader.readLine();

            if (lastName.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (!(UtilValidator.nameContainsValidCharacters(firstName) &&
                    UtilValidator.nameContainsValidCharacters(middleName) &&
                    UtilValidator.nameContainsValidCharacters(lastName)))
                writer.write(getString("names_letters_error") + "\n");
            else
                break;
        }

        String idNumber;
        while (true)
        {
            writer.write(getString("insert_id_number"));
            idNumber = reader.readLine();

            if (idNumber.equals(QUIT_ANY_TIME_OPTION))
                return;

            if (UtilValidator.isNumber(idNumber))
                break;

            writer.write(getString("invalid_input") + "\n");
        }

        User u = new User(firstName,
                middleName,
                lastName,
                Integer.parseInt(age),
                idNumber,
                email,
                username,
                password);

        userService.insertUser(u);
        writer.write(getString("register_success") + "\n");
        redirectTo(LOGIN_MENU_NODE);
    }

    public void exit()
    {
        stopProgram();
    }
}
