package com.isoft.intern.view.menus;

import com.isoft.intern.casino.games.blackjack.BlackJackGame;
import com.isoft.intern.casino.games.blackjack.Card;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.casino.games.blackjack.multiplayer.BlackJackClient;
import com.isoft.intern.casino.games.blackjack.multiplayer.SocketResponse;
import com.isoft.intern.casino.games.blackjack.player.entities.*;
import com.isoft.intern.connection.ClientSocket;
import com.isoft.intern.model.User;
import com.isoft.intern.userholder.LoggedInUser;
import com.isoft.intern.utils.StringUtil;
import com.isoft.intern.utils.validator.UtilValidator;
import org.fusesource.jansi.Ansi;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.isoft.intern.view.UserInputHandler.*;

/**
 * @author Stefan Ivanov
 */
public class BlackjackMenu
{
    private double userBet;

    private Logger LOGGER = Logger.getLogger(BlackjackMenu.class.getName());

    /**
     * starts a blackjack game that communicates with players through sockets
     */
    public void hostGame()
    {
        Thread serverThread = null;
        try
        {
            BlackJackGame g = new BlackJackGame();
            serverThread = new Thread(g);
            serverThread.start();

            writer.write(
                    String.format("Your IP is: %s%nPlayers need to enter it to join your game",
                            getIpAddress()));


        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            e.printStackTrace();
            writer.write(getString("could_not_create_server") + "\n" + e.getMessage());
            redirectTo("user_menu");
        }

        //until the server thread is running, keep the main thread in a standby mode
        while (serverThread == null || !serverThread.isInterrupted())
        {
            try
            {
                Thread.sleep(2000);
            } catch (InterruptedException ignored)
            {

            }
        }
        redirectTo("user_menu");
    }

    /**
     * Open a socket connection to the desired server host's IP
     */
    public void joinGame()
    {
        writer.write(getString("enter_ip_to_join"));
        String ip = reader.readLine();//getIpAddress();
        int port = 7777;
        ClientSocket socket = new ClientSocket(port, ip);
        Thread clientThread = null;
        try
        {
            socket.openConnection();
            BlackJackClient client = new BlackJackClient(socket, this);
            clientThread = new Thread(client);
            clientThread.start();
        } catch (IOException e)
        {
            redirectTo("user_menu");
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            writer.write(getString("connection_error_2")+"\n");
            e.printStackTrace();
        }

        try {
            Thread.sleep(99999999);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (clientThread != null)
            clientThread.interrupt();
        redirectTo("user_menu");
    }

    /**
     * @return the logged in user
     */
    public User getUser()
    {
        return LoggedInUser.getInstance().getCurrentlyLoggedUser();
    }


    /**
     * inputs user bet and removes it from the logged in user
     *
     * @return the bet amount
     */
    public Double inputUserBet()
    {
        String input;
        while (true)
        {
            writer.write(getString("wish_bet"));
            input = reader.readLine();
            if (input.equals("Q"))
                redirectTo("user_menu");
            if (!UtilValidator.isDoubleMoney(input))
                writer.write(getString("number_is_not_double_error") + "\n");
            else
            {
                User user = getUser();

                if (user.getAvailableMoney() < Double.parseDouble(input))
                {
                    writer.write(getString("not_enough_money_bet_error") + "\n");
                    writer.write(getString("available_money") + user.getAvailableMoney() + "\n");
                } else
                {
                    user.setAvailableMoney(user.getAvailableMoney() - Double.parseDouble(input));
                    userService.updateUser(user);
                    writer.write(getString("available_money_after_bet") + user.getAvailableMoney() + "\n");
                    userBet = Double.parseDouble(input);
                    return userBet;
                }
            }
        }
    }


    /**
     * renders the game by a given gameStatus map
     *
     * @param gameStatus the game status
     */
    public void printGame(Map<BlackJackEntity, BlackJackEntityStatus> gameStatus)
    {
        //gameStatus is a list of all players and dealer

        writer.write("\n-----------------------------------------------------------------\n");
        //Maybe this will work in terminal. should flush the console
        Ansi a = new Ansi();
        a.eraseLine(Ansi.Erase.ALL);
        a.eraseScreen();
        System.out.print("\033[H\033[2J");
        System.out.flush();

        String dealerCards = "";
        String dealerInfo = "";
        List<String> otherPlayersCards = new ArrayList<>();
        List<String> otherPlayersInfo = new ArrayList<>();
        Iterator<Map.Entry<BlackJackEntity, BlackJackEntityStatus>> it = gameStatus.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry<BlackJackEntity, BlackJackEntityStatus> entry = it.next();
            BlackJackEntity entity = entry.getKey();
            BlackJackEntityStatus status = entry.getValue();
            if (entity instanceof Dealer)
            {
                Dealer dealerEntity = (Dealer) entity;
                dealerCards = Card.getCardsInline(3, dealerEntity.getCards().toArray(new Card[0]));
                dealerInfo =
                        getPlayerInfo("DEALER", dealerEntity.calcScore(), status);
            } else if (entity instanceof BlackJackAI)
            {

                otherPlayersCards.add(Card.getCardsInline(3, entity.getCards().toArray(new Card[0])));
                otherPlayersInfo.add(getPlayerInfo("AI", entity.calcScore(), status));
            } else if (entity instanceof SplitPlayer)
            {
                BlackJackPlayer splitPlayer = (SplitPlayer) entity;
                otherPlayersInfo.add(getPlayerInfo("SPLIT", splitPlayer.calcScore(), status));
                otherPlayersCards.add(Card.getCardsInline(3, splitPlayer.getCards().toArray(new Card[0])));
            } else
            {
                BlackJackPlayer userPlayer = (BlackJackPlayer) entity;

                otherPlayersCards.add(Card.getCardsInline(3, entity.getCards().toArray(new Card[0])));
                otherPlayersInfo.add(getPlayerInfo(userPlayer.getPlayer().getUsername(), entity.calcScore(), status));

            }
        }

        StringUtil.printInline(0, dealerInfo, dealerCards);
        for (int i = 0; i < otherPlayersCards.size(); i++)
        {
            StringUtil.printInline(0, otherPlayersInfo.get(i), otherPlayersCards.get(i));
        }

    }


    /**
     * this method breaks single responsibility, it prints options to the user
     * appropriate to his current state and returns the user option as a socket response
     *
     * @param canSplit  if the user has the split option
     * @param canDouble if the user can double
     * @param hasSplit  if the user has already split
     * @return the user response (HIT,STAND,DOUBLE etc etc)
     */
    public SocketResponse printOptions(boolean canSplit, boolean canDouble, boolean hasSplit)
    {
        if (hasSplit)
        {
            writer.write("1. " + getString("blackjack_stand") + "\n");
            writer.write("2. " + getString("blackjack_hit") + "\n");
            writer.write("3. " + getString("blackjack_stand_split") + "\n");
            writer.write("4. " + getString("blackjack_hit_split") + "\n");
        } else if (canSplit)
        {
            writer.write("1. " + getString("blackjack_stand") + "\n");
            writer.write("2. " + getString("blackjack_hit") + "\n");
            writer.write("3. " + getString("blackjack_double") + "\n");
            writer.write("4. " + getString("blackjack_surrender") + "\n");
            writer.write("5. " + getString("blackjack_split") + "\n");
        } else if (canDouble)
        {
            writer.write("1. " + getString("blackjack_stand") + "\n");
            writer.write("2. " + getString("blackjack_hit") + "\n");
            writer.write("3. " + getString("blackjack_double") + "\n");
            writer.write("4. " + getString("blackjack_surrender") + "\n");
        } else
        {
            writer.write("1. " + getString("blackjack_stand") + "\n");
            writer.write("2. " + getString("blackjack_hit") + "\n");
            writer.write("3. " + getString("blackjack_surrender") + "\n");
        }

        return handleUserInput(canSplit, canDouble, hasSplit);
    }

    public SocketResponse askUserToJoin()
    {
        writer.write(getString("ask_to_join") + "\n");
        if (reader.readLine().equalsIgnoreCase("Y"))
            return SocketResponse.CONFIRM_JOIN;
        else return SocketResponse.CANCEL_JOIN;

    }


    public void invalidResponse()
    {
        writer.write(getString("invalid_response_something_went_wrong"));
    }

    public void requestExpired()
    {
        writer.write(getString("request_expired_added_to_waiting_queue"));
    }

    /**
     * Game has ended, write to the user if he has won or lost and the reward
     * i should have made a separate enum for possible outcomes (WIN,LOSE,DRAW)
     * but i have too many classes and this project is getting to big anyway so...
     *
     * @param outcome the outcome of the game (WIN,LOSE,DRAW)
     * @param reward  the reward;
     */
    public void gameEnded(BlackJackEntityStatus outcome, double reward)
    {
        writer.write(getString("game_has_ended") + "\n");
        switch (outcome)
        {
            case WIN:
            {
                writer.write("user money before : " + getUser().getAvailableMoney() + "\n");

                writer.write(getString("won") + "\n");
                writer.write(getString("you_have_won") + " " + reward);

                getUser().setAvailableMoney(getUser().getAvailableMoney() + reward);
                userService.updateUser(getUser());

                writer.write("user money after reward: " + getUser().getAvailableMoney() + "\n");
            }
            break;
            case LOSE:
            {
                writer.write(getString("lose") + "\n");
            }
            break;
            case DRAW:
            {
                //This will automaticly update the logged in user instance too
                getUser().setAvailableMoney(getUser().getAvailableMoney() + reward);
                userService.updateUser(getUser());

                writer.write(getString("draw") + "\n");
                writer.write(getString("refund") + " " + reward);

            }
            break;
        }
    }

    /**
     * this method works behind the scenes, refunds the user the amount
     * called when the user failed to place his bet in the required game state
     *
     * @param refundAmount the refund amount :)
     */
    public void refund(Double refundAmount)
    {
        getUser().setAvailableMoney(getUser().getAvailableMoney() + refundAmount);
        userService.updateUser(getUser());
    }

    private SocketResponse handleUserInput(boolean canSplit, boolean canDouble, boolean hasSplit)
    {
        while (true)
        {
            String input = reader.readLine();

            if (canSplit)
                switch (input)
                {
                    case "1":
                        return SocketResponse.STAND;
                    case "2":
                        return SocketResponse.HIT;
                    case "3":
                    {
                        //removes the previous bet from the user's money
                        boolean enoughToDouble=false;
                        if(getUser().getAvailableMoney()-userBet>0)
                            enoughToDouble=true;
                        else
                            writer.write(getString("bet_money_error")+"\n");

                        if(enoughToDouble)
                        {
                            getUser().setAvailableMoney(getUser().getAvailableMoney() - userBet);
                            userService.updateUser(getUser());
                            return SocketResponse.DOUBLE;
                        }
                    }
                    case "4":
                        return SocketResponse.SURRENDER;
                    case "5":
                        return SocketResponse.SPLIT;
                    default:
                        writer.write("invalid_input");
                }
            else if (hasSplit)
                switch (input)
                {
                    case "1":
                        return SocketResponse.STAND;
                    case "2":
                        return SocketResponse.HIT;
                    case "3":
                        return SocketResponse.STAND_SPLIT;
                    case "4":
                        return SocketResponse.HIT_SPLIT;
                }
            else if (canDouble)
                switch (input)
                {
                    case "1":
                        return SocketResponse.STAND;
                    case "2":
                        return SocketResponse.HIT;
                    case "3":
                    {
                        //removes the previous bet from the user's money
                        boolean enoughToDouble=false;
                        if(getUser().getAvailableMoney()-userBet>0)
                            enoughToDouble=true;
                        else
                            writer.write(getString("bet_money_error")+"\n");
                        if(enoughToDouble)
                        {
                            getUser().setAvailableMoney(getUser().getAvailableMoney() - userBet);
                            userService.updateUser(getUser());
                            return SocketResponse.DOUBLE;
                        }
                    }
                    case "4":
                        return SocketResponse.SURRENDER;
                }
            else
                switch (input)
                {
                    case "1":
                        return SocketResponse.STAND;
                    case "2":
                        return SocketResponse.HIT;
                    case "3":
                        return SocketResponse.SURRENDER;
                }
        }
    }

    private String getPlayerInfo(String playerName, int score, BlackJackEntityStatus action)
    {
        //Its 11 because it doesnt have "\n" newline symbol
        String player = StringUtil.getStringLeftAligned(playerName, 11, 0);
        String pScore = StringUtil.getStringLeftAligned("\nScore: " + score, 12, 0);
        String actionString = StringUtil.getStringLeftAligned("\n" + action.toString(), 12, 0);

        return player + pScore + actionString;
    }

    private String getIpAddress()
    {
        try (final DatagramSocket datagramSocket = new DatagramSocket())
        {
            datagramSocket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            String ip = datagramSocket.getLocalAddress().getHostAddress();

            return ip;
        } catch (SocketException | UnknownHostException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}

