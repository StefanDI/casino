package com.isoft.intern.view;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing single MenuOption in the view tree
 * It has a parent and/or redirectedFrom, accessed when back option is chosen
 * (redirectedFrom has higher priority than parent when navigating back)
 * children represents all options available while in the current menu
 * content is the 'name' of the node
 * if its a leaf(ActionNode its the name of the method to invoke
 * if its not a leaf then content is the name of the node
 * the name of the node is used for
 * 1)Navigating to this node
 * 2)Represents a key from locale_{chosen locale}.json file for translation
 */

public class MenuNode
{
    MenuNode redirectedFrom;

    MenuNode parent;

    List<MenuNode> children;

    String content;

    boolean isLeaf;

    MenuNode(String content)
    {
        this.content = content;

        isLeaf = false;
        children = new ArrayList<MenuNode>();
    }

    public MenuNode(MenuNode copy)
    {
        redirectedFrom = copy.redirectedFrom;
        content = copy.content;
        children = copy.children;
        parent = copy.parent;
        isLeaf = copy.isLeaf;
    }
}