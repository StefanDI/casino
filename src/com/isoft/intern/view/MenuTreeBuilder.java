package com.isoft.intern.view;

import com.isoft.intern.model.UserRoles;
import com.isoft.intern.userholder.LoggedInUser;
import com.isoft.intern.view.exceptions.NodeNotFoundException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

/**
 * Given a path to a json file representing menu (in our case MenuArchitecture.json)
 * Builds a tree of {@link MenuNode}s
 * exposes:
 * {@link #getNodeByName(String)} returns a node by its name
 * {@link #buildMenuMap(MenuNode)} builds a menu map from the given node's children
 * menu map is used to represent each child with a number (as option from the console)
 *
 * @author Stefan Ivanov
 */
public class MenuTreeBuilder
{

    private List<MenuNode> trees;

    public MenuTreeBuilder(String filePath) throws IOException
    {
        trees = new ArrayList<>();
        loadMenuHierarchy(filePath);
    }

    /**
     * returns the given MenuNode by its content
     *
     * @param name the content of the view node
     * @return the view node
     * @throws NodeNotFoundException if there is no such view node
     */
    public MenuNode getNodeByName(String name) throws NodeNotFoundException
    {
        for (MenuNode root : trees)
        {
            Queue<MenuNode> MenuNodes = new ArrayDeque<>();
            MenuNodes.add(root);

            MenuNode n = performBFS(MenuNodes, name);
            if (n != null)
            {
                return n;
            }
        }
        throw new NodeNotFoundException("MenuNode not found " + name);
    }

    /**
     * Builds a map with all the children of the given MenuNode
     *
     * @param currentNode the view node which's children we extrat
     * @return the map
     */
    Map<Integer, MenuNode> buildMenuMap(MenuNode currentNode)
    {
        Map<Integer, MenuNode> map = new HashMap<>();
        int i = 1;
        for (MenuNode child : currentNode.children)
        {
            //Make a copy of the node because isVisibleToCurrentUser method replaces
            // expressions inside node content
            MenuNode node = new MenuNode(child);
            if (isVisibleToCurrentUser(node))
                map.put(i++, node);
        }

        return map;
    }

    /**
     * BFS search for the tree
     *
     * @param nodes    the queue with nodes
     * @param nodeName string to match
     * @return the found node, and null if there is no such
     */
    private MenuNode performBFS(Queue<MenuNode> nodes, String nodeName)
    {
        while (!nodes.isEmpty())
        {
            MenuNode n = nodes.poll();
            if (n.content.trim().toLowerCase().equals(
                    nodeName.trim().toLowerCase()))
            {
                return n;
            } else
            {
                nodes.addAll(n.children);
                return performBFS(nodes, nodeName);
            }
        }
        return null;

    }

    private void loadMenuHierarchy(String filePath) throws IOException
    {
        RandomAccessFile file = new RandomAccessFile(filePath, "r");

        String line;
        StringBuilder allLines = new StringBuilder();
        while ((line = file.readLine()) != null)
        {
            allLines.append(line);
        }

        JSONObject object = new JSONObject(allLines.toString());

        file.close();
        makeTree(object);
    }


    private void makeTree(JSONObject allMenus)
    {
        //Initialize all the roots
        //Every key is a different "view phase"
        //currently there is
        // main_menu and user_menu
        for (Iterator<String> it = allMenus.keys(); it.hasNext(); )
        {
            String key = it.next();
            //Initialize root node
            MenuNode root = new MenuNode(key);
            //since its the root, parent is null
            root.parent = null;
            //make tree recursively from root and its children in JSON format
            makeTree(root, allMenus.getJSONObject(key));

            trees.add(root);

        }
    }

    /**
     * Recursive function for initializing the tree
     *
     * @param parent the last MenuNode in the tree
     * @param self   the current JSONObject containing all the children of the node (in the json file)
     */
    private void makeTree(MenuNode parent, JSONObject self)
    {
        //Bottom of the recursion
        if (self == null)
            return;

        //Iterate over all the keys of the current view node
        for (String menuOption : self.keySet())
        {
            MenuNode currentNode;
            try
            {
                //If the current key(representing view item)
                //has children, then it has submenu
                JSONObject children = self.getJSONObject(menuOption);
                //Instantiate node
                currentNode = new MenuNode(menuOption);
                //Make the parent link to child, and child link to parent
                parent.children.add(currentNode);
                currentNode.parent = parent;
                //go into recursion
                makeTree(currentNode, children);
            } catch (JSONException endOfTree)
            {
                //Else the current key(representing view item)
                //doesn't have children, so its value is
                //the name of the method to invoke
                currentNode = new MenuNode(menuOption);

                parent.children.add(currentNode);
                currentNode.parent = parent;
                //This value will represent the method that needs to be called after
                //successful verification
                String leafNodeValue = self.getString(menuOption);
                MenuNode leafNode = new MenuNode(leafNodeValue);
                leafNode.isLeaf = true;
                currentNode.children.add(leafNode);
                //Call recursion with null second parameter
                //this will exit recursion
                makeTree(currentNode, null);
            }
        }
    }

    private boolean isVisibleToCurrentUser(MenuNode node)
    {
        if (!node.content.contains("(") && !node.content.contains(")"))
            return true;
        if (!LoggedInUser.getInstance().isLoggedIn())
            return true;

        String expression = node.content.substring(1, node.content.indexOf(")"));
        String[] tokens = expression.split(":");
        String leftSide = tokens[0];
        String rightSide = tokens[1];

        if (leftSide.toLowerCase().equals("hasrole"))
        {
            UserRoles role = UserRoles.valueOf(rightSide);
            //If not logged in and role is NOT PLAYER
            if (!LoggedInUser.getInstance().isLoggedIn() && !role.equals(UserRoles.PLAYER))
            {
                node.content = node.content.substring(node.content.indexOf(")") + 1);
                return true;
            }
            if (LoggedInUser.getInstance().isLoggedIn() &&
                    LoggedInUser.getInstance().getCurrentlyLoggedUser().getUserRole().equals(role))
            {
                node.content = node.content.substring(node.content.indexOf(")") + 1);
                return true;
            }
        }

        return false;
    }

}
