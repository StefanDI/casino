package com.isoft.intern.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;

import static com.isoft.intern.casino.games.roulette.state.context.RouletteContext.LOGGER;

/**
 * This class represents client socket
 * <p>
 *
 *
 * @author Stefan Ivanov
 */
public class ClientSocket
{
    private int port;
    private String ipAddress;
    private Socket clientSocket;
    private ObjectInputStream in;
    private ObjectOutputStream out;

    public ClientSocket(Socket socket)
    {
        this.clientSocket = socket;
    }

    public ClientSocket(int port, String ipAddress)
    {
        this.port = port;
        this.ipAddress = ipAddress;
    }

    public void writeToServer(Object o) throws IOException
    {
        if (out == null)
            out = new ObjectOutputStream(clientSocket.getOutputStream());
        out.writeObject(o);

    }

    public Object awaitServerResponse() throws IOException, ClassNotFoundException
    {

        if (in == null)
            in = new ObjectInputStream(clientSocket.getInputStream());
        return in.readObject();

    }

    public Socket getSocket()
    {
        return clientSocket;
    }

    public void openConnection() throws IOException
    {
        this.clientSocket = new Socket(ipAddress, port);
        clientSocket.setKeepAlive(true);
        clientSocket.setTcpNoDelay(true);
    }

    public void close()
    {
        try
        {
            clientSocket.close();
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage()+"ClientSocket class,row:66",e);
        }
    }
}
