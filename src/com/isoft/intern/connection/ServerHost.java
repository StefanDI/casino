package com.isoft.intern.connection;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.logging.Level;

import static com.isoft.intern.casino.games.roulette.state.context.RouletteContext.LOGGER;

/**
 * this class is the host for a blackjack game
 * stores a list of sockets for each player connected to the game
 * sends messages to sockets
 *
 * @author Stefan Ivanov
 */
public class ServerHost extends Observable implements Runnable
{
    private final ServerSocket serverSocket;

    private boolean isRunning;
    private List<ClientSocket> clientSockets;
    private Map<ClientSocket, ObjectInputStream> clientInStreams;
    private Map<ClientSocket, ObjectOutputStream> clientOutStreams;


    public ServerHost(int port) throws IOException
    {
        clientInStreams = new HashMap<>();
        clientOutStreams = new HashMap<>();
        isRunning = true;
        clientSockets = new ArrayList<>();
        this.serverSocket = new ServerSocket(port);
    }

    @Override
    public void run()
    {
        while (isRunning)
            acceptSockets();
    }

    /**
     * Sends an object to the desired socket
     *
     * @param s the socket
     * @param o the object
     * @throws IOException            if there is an IO error with the socket
     * @throws IllegalAccessException if the socket is not in the list, it was most likely removed
     */
    public void writeToSocket(ClientSocket s, Object o) throws IOException, IllegalAccessException
    {
        if (!clientSockets.contains(s))
            throw new IllegalAccessException("No connection to this socket: " + s.toString());

        if (clientOutStreams.get(s) == null)
            clientOutStreams.put(s, new ObjectOutputStream(s.getSocket().getOutputStream()));


        clientOutStreams.get(s).writeObject(o);

        LOGGER.log(Level.WARNING,"Object written to socket.");
    }

    /**
     * Waits for a response from the desired socket
     *
     * @param s the socket
     * @return the object response from the socket
     * @throws IllegalAccessException if there is no connection to the socket(it was most probably removed, when the host tried to write to all sockets)
     * @throws IOException            if there is an IO issue with the socket
     * @throws ClassNotFoundException is thrown from the ObjectInputSStream
     */
    public Object listenToSocket(ClientSocket s) throws IllegalAccessException, IOException, ClassNotFoundException
    {
        if (!clientSockets.contains(s))
            throw new IllegalAccessException("No connection to this socket: " + s.toString());

        if (clientInStreams.get(s) == null)
            clientInStreams.put(s, new ObjectInputStream(s.getSocket().getInputStream()));

        return clientInStreams.get(s).readObject();
    }


    /**
     * Sends an object to all the sockets
     * if there is an error with a socket, deletes it from the list
     * usually used when sending render info to all players
     *
     * @param o the object to be sent to all sockets
     */
    public void writeToAllSockets(Object o)
    {
        Iterator<ClientSocket> it = clientSockets.iterator();
        while (it.hasNext())
        {
            ClientSocket clientSocket = it.next();
            try
            {
                writeToSocket(clientSocket, o);
            } catch (IOException | IllegalAccessException e)
            {
                //Something was wrong with the connection, ignore it and remove the socket
                it.remove();
                clientInStreams.remove(clientSocket);
                clientOutStreams.remove(clientSocket);
            }
        }

    }

    private void acceptSockets()
    {
        try
        {
            Socket s = serverSocket.accept();
            ClientSocket socket = new ClientSocket(s);
            clientSockets.add(socket);
            setChanged();
            notifyObservers(socket);
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            e.printStackTrace();
        }
    }

}
