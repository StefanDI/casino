package com.isoft.intern.userholder;

import com.isoft.intern.model.User;

import java.util.Observable;
import java.util.Observer;

/**
 * @author Petar Hristov
 * <p>
 * Holds the logged in user as a singleton.
 * Gets notified via {@link #update(Observable, Object)} when changes are made to the user in the database
 */
public class LoggedInUser implements Observer
{

    // private instance, so that it can be
    // accessed by only by getInstance() method
    private static LoggedInUser instance;

    private User currentlyLoggedUser;

    private LoggedInUser()
    {
        // private constructor
    }

    //synchronized method to control simultaneous access
    synchronized public static LoggedInUser getInstance()
    {
        if (instance == null)
        {
            // if instance is null, initialize
            instance = new LoggedInUser();
        }
        return instance;
    }

    public boolean isLoggedIn()
    {
        return this.currentlyLoggedUser != null;
    }

    public User getCurrentlyLoggedUser()
    {
        return this.currentlyLoggedUser;
    }

    public void logoutCurrentUser()
    {
        this.currentlyLoggedUser = null;
    }

    @Override
    public void update(Observable observable, Object o)
    {
        if (o instanceof User)
            this.currentlyLoggedUser = (User) o;

    }
}
