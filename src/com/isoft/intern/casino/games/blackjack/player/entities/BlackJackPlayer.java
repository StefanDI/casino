package com.isoft.intern.casino.games.blackjack.player.entities;

import com.isoft.intern.casino.games.blackjack.Card;
import com.isoft.intern.connection.ClientSocket;
import com.isoft.intern.model.User;

import java.util.Objects;

/**
 * this class is every user player connected to the server
 *
 * @author Stefan Ivanov
 */
public class BlackJackPlayer extends BlackJackEntity
{
    private ClientSocket socket;
    private final User player;
    private boolean hasSplit;

    /**
     * the server side constructor
     *
     * @param player the user
     * @param socket the socket its communicating on
     */
    public BlackJackPlayer(User player, ClientSocket socket)
    {
        this.socket = socket;
        this.player = player;
    }

    /**
     * This constructor is the view model of the blackjack player
     *
     * @param player
     */
    public BlackJackPlayer(User player)
    {
        this.player = player;
    }

    public boolean hasBlackjack()
    {
        return calcScore() == 21 && cards.size() == 2;
    }

    public boolean canSplit()
    {
        return this.cards.size() == 2 && cards.get(0).card.equals(cards.get(1).card);
    }

    public void setHasSplit(boolean hasSplit)
    {
        this.hasSplit = hasSplit;
    }

    public boolean getHasSplit()
    {
        return hasSplit;
    }

    public void addCard(Card card)
    {
        cards.add(card);
    }

    public void resetCards()
    {
        this.cards.clear();
    }

    public ClientSocket getSocket()
    {
        return socket;
    }

    public User getPlayer()
    {
        return player;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BlackJackPlayer player1 = (BlackJackPlayer) o;
        return Objects.equals(player, player1.player);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(super.hashCode(), player);
    }
}
