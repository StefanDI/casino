package com.isoft.intern.casino.games.blackjack.player.entities;

import com.isoft.intern.casino.games.blackjack.TurnHandler;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.casino.games.blackjack.multiplayer.SocketResponse;

public class BlackJackAI extends BlackJackPlayer
{
    private static final int SIMULATE_THINKING_TIME = 500;

    public BlackJackAI()
    {
        super(null);
    }

    /**
     * simple if/else AI choice
     * TODO: neural networks :P :P:P:P:P:P:P:P:P:PPP:P:P
     *
     * @param turnHandler handles AI requests
     */
    public void takeTurn(TurnHandler turnHandler)
    {
        while (calcScore() < 21)
        {
            try
            {
                Thread.sleep(SIMULATE_THINKING_TIME);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            if (calcScore() >= 18)
            {
                turnHandler.handleTurn(SocketResponse.STAND, this);
                changeStatus(BlackJackEntityStatus.STAND);
                break;
            } else if (calcScore() == 16)
            {
                turnHandler.handleTurn(SocketResponse.SURRENDER, this);
                changeStatus(BlackJackEntityStatus.SURRENDER);
                break;
            } else
            {
                turnHandler.handleTurn(SocketResponse.HIT, this);
                changeStatus(BlackJackEntityStatus.HIT);
                break;
            }
        }
    }


}
