package com.isoft.intern.casino.games.blackjack.player.entities;

import com.isoft.intern.casino.games.blackjack.Card;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the father class of all entities (Dealers and players)
 *
 * @author Stefan Ivanov
 */
public abstract class BlackJackEntity
{
    protected List<Card> cards;
    private BlackJackEntityStatus status;

    BlackJackEntity()
    {
        status = BlackJackEntityStatus.WAIT;
        this.cards = new ArrayList<>();
    }

    /**
     * this is for render purposes only
     *
     * @return the current status of the entity
     */
    public BlackJackEntityStatus getStatus()
    {
        return status;
    }

    public void changeStatus(BlackJackEntityStatus newStatus)
    {
        this.status = newStatus;
    }

    public List<? extends Card> getCards()
    {
        return cards;
    }

    /**
     * calculates the score of the entity, taking into account that aces can be count as 11 or 1
     *
     * @return the score of the entity
     */
    public int calcScore()
    {
        int sum = 0;

        List<Card> sortedCards = new ArrayList<>(cards);

        //Put a custom comparator, to put aces in the end
        //We count the cards score left to right
        // so when we decide to count ace for 1 or 10, we know for sure next score wont exceed 21
        sortedCards.sort((c1, c2) -> {
            if (c1.card.equals("A") && c2.card.equals("A"))
                return 0;
            if (c1.card.equals("A"))
                return 1;
            return -1;
        });

        for (Card card : sortedCards)
        {
            //If we bust with ace as 11, we count it as 1
            if (card.card.equals("A"))
            {
                if (sum + 11 > 21)
                    sum += 1;
                else
                    sum += 11;
            } else if (card.card.equals(Card.UNKNOWN))
            {
                //Ignore
                continue;
            } else
            {
                //          J, Q K or 10
                sum += card.isTenPoints() ? 10 : Integer.parseInt(card.card);
            }
        }

        return sum;
    }

    public void setCards(List<Card> cards)
    {
        this.cards = cards;
    }

}
