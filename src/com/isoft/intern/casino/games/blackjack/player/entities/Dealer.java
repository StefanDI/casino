package com.isoft.intern.casino.games.blackjack.player.entities;

import com.isoft.intern.casino.games.blackjack.BlackJackGame;
import com.isoft.intern.casino.games.blackjack.Card;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;

import java.util.*;

/**
 * @author Stefan Ivanov
 */
public class Dealer extends BlackJackEntity
{
    private static final int MAX_DRAW_SCORE = 17;

    //A map of <Card, isVisible> for hole card logic
    private final Map<Card, Boolean> cardsWithVisibility;

    private Queue<Card> deque;
    private BlackJackGame game;

    public Dealer(Queue<Card> deque, BlackJackGame game)
    {
        this.game = game;
        this.deque = deque;
        this.cardsWithVisibility = new HashMap<>();
    }

    public boolean hasAceFirstCard()
    {
        for (Card card : cardsWithVisibility.keySet())
        {
            //If there is an ace return its value from the map
            //If its false, it means the ace is not visible so we still return false
            if (card.card.equalsIgnoreCase("A"))
                return cardsWithVisibility.get(card);
        }

        return false;
    }


    public void dealCard(BlackJackPlayer toPlayer)
    {
        toPlayer.addCard(deque.poll());
    }


    public void dealHands()
    {
        int NUMBER_OF_CARDS_PER_PLAYER = 2;
        for (int i = 0; i < NUMBER_OF_CARDS_PER_PLAYER; i++)
        {
            for (BlackJackPlayer player : game.getPlayers())
            {
                player.addCard(deque.poll());
            }


            //first iteration deal visible card, second iteration deal hidden(hole card)
            boolean isHoleCard = i == NUMBER_OF_CARDS_PER_PLAYER - 1;
            //since the map is <Card, isVisible> we put !isHoleCard, because hole card is NOT visible
            Card dealerCard = deque.poll();
            cardsWithVisibility.put(dealerCard, !isHoleCard);
            cards.add(dealerCard);
        }
    }

    public Map<Card, Boolean> getAllCards()
    {
        return cardsWithVisibility;
    }

    /**
     * Get a list of cards, replacing the hole card(if any) with unknown one
     *
     * @return The list of dealer cards, but replace the holeCard with Card.UNKNOWN
     */
    @Override
    public List<Card> getCards()
    {
        List<Card> dealerCards = new ArrayList<>();
        for (Card card : cardsWithVisibility.keySet())
        {

            int dealerCardHeight = 3;
            card.setCardHeight(dealerCardHeight);
            //If its visible
            if (cardsWithVisibility.get(card))
                dealerCards.add(card);
            else
                dealerCards.add(new Card(Card.UNKNOWN, Card.UNKNOWN, dealerCardHeight));
        }

        return dealerCards;
    }


    public void showHoleCard()
    {
        for (Card card : cardsWithVisibility.keySet())
        {
            if (!cardsWithVisibility.get(card))
                cardsWithVisibility.put(card, true);
        }
    }

    public void takeTurn(BlackJackGame game)
    {
        while (calcScore() <= MAX_DRAW_SCORE)
        {
            Card card = deque.poll();
            cardsWithVisibility.put(card, true);
            cards.add(card);

            game.render();
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        if (calcScore() == 21)
        {
            changeStatus(BlackJackEntityStatus.BLACKJACK);
            game.render();
        } else if (calcScore() > 21)
        {
            changeStatus(BlackJackEntityStatus.BUST);
            game.render();
        } else
        {
            changeStatus(BlackJackEntityStatus.HIT);
            game.render();
        }
    }

    @Override
    public void setCards(List<Card> cards)
    {
        //Dealer overrides the method so all cards are visible
        this.cards = cards;
        cardsWithVisibility.clear();
        for (Card card : cards)
        {
            cardsWithVisibility.put(card, true);
        }
    }
}
