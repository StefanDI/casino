package com.isoft.intern.casino.games.blackjack;

import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.casino.games.blackjack.multiplayer.SocketResponse;
import com.isoft.intern.casino.games.blackjack.player.entities.BlackJackPlayer;
import com.isoft.intern.casino.games.blackjack.player.entities.SplitPlayer;

public class TurnHandler {
    private BlackJackGame game;

    public TurnHandler(BlackJackGame game)
    {
        this.game = game;
    }

    public boolean handleTurn(SocketResponse choice, BlackJackPlayer player)
    {
        switch (choice)
        {
            case HIT:
                player.changeStatus(BlackJackEntityStatus.HIT);

                game.getDealer().dealCard(player);
                game.render();
                break;

            case STAND:
                player.changeStatus(BlackJackEntityStatus.STAND);
                game.render();
                return true;

            case DOUBLE:
                player.changeStatus(BlackJackEntityStatus.DOUBLE);

                game.getDealer().dealCard(player);
                Double bet = game.getPlayersByBet().get(player);
                game.getPlayersByBet().put(player,bet*2);
                game.render();
                return true;

            case SPLIT:
                player.changeStatus(BlackJackEntityStatus.SPLIT);
                player.setHasSplit(true);
                game.addSplit(player);
                game.render();
                break;

            case SURRENDER:
                player.changeStatus(BlackJackEntityStatus.SURRENDER);
                game.render();
                return true;


            case HIT_SPLIT:
                SplitPlayer split = game.getSplit(player);
                game.getDealer().dealCard(split);
                split.changeStatus(BlackJackEntityStatus.HIT);
                if (split.calcScore() == 21)
                    split.changeStatus(BlackJackEntityStatus.BLACKJACK);
                else if (split.calcScore() > 21)
                    split.changeStatus(BlackJackEntityStatus.BUST);
                game.render();
                break;

            case STAND_SPLIT:
                SplitPlayer splitPlayer = game.getSplit(player);
                splitPlayer.changeStatus(BlackJackEntityStatus.STAND);
                game.render();
                return true;
        }

        return false;
    }
}
