package com.isoft.intern.casino.games.blackjack;

import com.isoft.intern.utils.ANSI;
import com.isoft.intern.utils.StringUtil;

import java.util.Objects;

/**
 * @author Stefan Ivanov
 */
public class Card
{
    public static final String HEARTS = ANSI.ANSI_RED + '\u2665' + ANSI.ANSI_RESET;
    public static final String DIAMONDS = ANSI.ANSI_YELLOW + '\u2666' + ANSI.ANSI_RESET;
    public static final String SPADES = ANSI.ANSI_BLACK + '\u2660' + ANSI.ANSI_RESET;
    public static final String CLUBS = ANSI.ANSI_BOLD + ANSI.ANSI_BRIGHT_BLACK + '\u2663' + ANSI.ANSI_RESET;
    public static final String UNKNOWN = " " + ANSI.ANSI_RESET;

    private static final int DEFAULT_CARD_HEIGHT = 7;

    private Integer cardHeight;
    public String card;
    public String suit;

    public Card(String suit, String card, int cardHeight)
    {
        if (!suit.equals(CLUBS) &&
                !suit.equals(DIAMONDS) &&
                !suit.equals(HEARTS) &&
                !suit.equals(SPADES) &&
                !suit.equals(UNKNOWN))
            throw new IllegalArgumentException("INVALID SUIT!!!");

        //Card height less than 2 is not supported
        if (cardHeight <= 2)
            this.cardHeight = DEFAULT_CARD_HEIGHT;
        else
            this.cardHeight = cardHeight;
        this.suit = suit;
        this.card = card;
    }

    public Card(String suit, String card)
    {
        this(suit, card, DEFAULT_CARD_HEIGHT);
    }

    public static String getCardsInline(Card... cards)
    {
        String indentBetweenCards = " ";


        StringBuilder sb = new StringBuilder();

        String[][] cardsToString = new String[cards.length][];
        for (int i = 0; i < cards.length; i++)
        {
            cardsToString[i] = cards[i].getCardString().split("\n");
        }

        StringUtil.printInline(indentBetweenCards, sb, cardsToString);

        return sb.toString();
    }

    public static String getCardsInline(int newSize, Card... cards)
    {
        for (Card card : cards)
        {
            card.setCardHeight(newSize);
        }
        return getCardsInline(cards);
    }

    public void setCardHeight(Integer cardHeight)
    {
        this.cardHeight = cardHeight;
    }


    public boolean isPairWith(Card card)
    {
        return this == card ||
                card.isTenPoints() && isTenPoints();
    }

    public boolean isTenPoints()
    {
        return card.equals("A") ||
                card.equals("K") ||
                card.equals("Q") ||
                card.equals("J");
    }
    //
    //Card drawing methods
    //

    public String getCardString()
    {
        StringBuilder sb = new StringBuilder();
        int cardWidth = cardHeight * 2;
        String background = suit.equals(UNKNOWN) ? ANSI.ANSI_BG_BLACK : ANSI.ANSI_BG_WHITE;
        String cardColor = suit.equals(UNKNOWN) ? ANSI.ANSI_RED : ANSI.ANSI_BLACK;
        String cardWithBackground = background + cardColor + card + ANSI.ANSI_RESET;
        String suitWithBackGround = background + suit;
        for (int row = 0; row < cardHeight; row++)
        {
            String halfCardFill = background + generateEmptyString(cardWidth / 2);
            String singleFieldFill = " ";
            if (row == 0)
            {
                singleFieldFill = background + " " + cardWithBackground;
                String leftHalfFill = background + generateEmptyString(cardWidth / 2 - 1);
                if (card.equals("10"))
                    leftHalfFill = background + generateEmptyString(cardWidth / 2 - 2);

                sb.append(singleFieldFill + leftHalfFill + halfCardFill + ANSI.ANSI_RESET + "\n");
            } else if (row % 2 == 1)
            {
                singleFieldFill = suitWithBackGround;
                sb.append(halfCardFill + singleFieldFill + halfCardFill + ANSI.ANSI_RESET + "\n");
            } else if (row == cardHeight - 1)
            {
                singleFieldFill = cardWithBackground + background + " ";
                String rightHalfFill = generateEmptyString(cardWidth / 2 - 1);

                if (card.equals("10"))
                    rightHalfFill = generateEmptyString(cardWidth / 2 - 2);

                sb.append(halfCardFill + rightHalfFill + singleFieldFill + ANSI.ANSI_RESET + "\n");
            } else
            {
                singleFieldFill = " ";
                sb.append(halfCardFill + singleFieldFill + halfCardFill + ANSI.ANSI_RESET + "\n");
            }

        }
        return sb.toString();
    }

    public String getEmptyCardSpace()
    {
        String emptyString = "";
        int cardWidth = cardHeight * 2;
        for (int row = 0; row < cardHeight; row++)
        {
            for (int col = 0; col < cardWidth; col++)
            {
                emptyString += " ";
            }
            emptyString += "\n";
        }
        return emptyString;
    }

    private String generateEmptyString(int spaces)
    {
        String str = "";
        for (int i = 0; i < spaces; i++)
        {
            str += " ";
        }
        return str;
    }

    public String getCardMirrored()
    {
        StringBuilder sb = new StringBuilder();
        String[] cardRows = getCardString().split("\n");

        for (int i = cardRows.length - 1; i >= 0; i--)
        {
            sb.append(cardRows[i] + ANSI.ANSI_RESET + "\n");
        }

        return sb.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card1 = (Card) o;
        return card.equals(card1.card) &&
                suit.equals(card1.suit);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(card, suit);
    }
}
