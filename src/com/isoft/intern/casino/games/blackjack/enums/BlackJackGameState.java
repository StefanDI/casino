package com.isoft.intern.casino.games.blackjack.enums;

/**
 * All the game states of a blackjack game
 */
public enum BlackJackGameState
{
    PREPARE,
    BET,
    DEAL,
    PLAY,
    EVALUATE,
    STOP;

}
