package com.isoft.intern.casino.games.blackjack.enums;

/**
 * This is intended for render purposes only
 * although its not only used for that :D
 */
public enum BlackJackEntityStatus
{
    WAIT,
    HIT,
    STAND,
    DOUBLE,
    SURRENDER,
    SPLIT,
    BUST,
    BLACKJACK,
    NATURAL,
    REVEAL_HOLE_CARD,
    DISCONNECTED,
    WIN,
    LOSE,
    DRAW;
}
