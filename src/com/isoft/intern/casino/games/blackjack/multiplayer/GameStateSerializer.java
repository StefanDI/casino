package com.isoft.intern.casino.games.blackjack.multiplayer;

import com.isoft.intern.casino.games.blackjack.Card;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.casino.games.blackjack.player.entities.*;
import com.isoft.intern.model.User;

import java.util.*;

/**
 * This class serializes and deserializes the game state map
 *
 * @author Stefan Ivanov
 */
public class GameStateSerializer
{

    private GameStateSerializer()
    {

    }


    /**
     * @param gameStatus the original game status
     * @return the serialized map, ready to be transfered through the socket
     */
    public static Map<Map<Object, List<String>>, BlackJackEntityStatus> serializeGameState(Map<BlackJackEntity, BlackJackEntityStatus> gameStatus)
    {
        if (gameStatus == null)
            return null;
        //Insert order is important!
        Map<Map<Object, List<String>>, BlackJackEntityStatus> serializedMap = new LinkedHashMap<>();

        for (Map.Entry<BlackJackEntity, BlackJackEntityStatus> entry : gameStatus.entrySet())
        {
            BlackJackEntity entity = entry.getKey();
            BlackJackEntityStatus status = entry.getValue();

            Object className = null;
            if (entity instanceof BlackJackPlayer && ((BlackJackPlayer) entity).getPlayer() != null)
            {
                className = ((BlackJackPlayer) entity).getPlayer();
            } else
            {
                className = entity.getClass().getSimpleName();
            }
            //card, suit comma separated
            List<String> cards = new ArrayList<>();

            for (Card card : entity.getCards())
            {
                String stringCard = card.card + "," + card.suit;
                cards.add(stringCard);
            }
            Map<Object, List<String>> classNameCardsPair = new HashMap<>();
            classNameCardsPair.put(className, cards);
            serializedMap.put(classNameCardsPair, status);
        }

        return serializedMap;
    }

    /**
     * @param gameState the serialized game state
     * @return the deserialized, view friendly map with game state
     */
    public static Map<BlackJackEntity, BlackJackEntityStatus> deserializeGameState(Map<Map<Object, List<String>>, BlackJackEntityStatus> gameState)
    {
        //Again insert order is important
        Map<BlackJackEntity, BlackJackEntityStatus> deserializedMap = new LinkedHashMap<>();

        for (Map.Entry<Map<Object, List<String>>, BlackJackEntityStatus> entry : gameState.entrySet())
        {
            Map<Object, List<String>> playerWithCards = entry.getKey();
            BlackJackEntityStatus status = entry.getValue();

            //Im sure its only 1 entry but unfortunately there isnt a pair class like c++
            for (Map.Entry<Object, List<String>> listEntry : playerWithCards.entrySet())
            {
                Object className = listEntry.getKey();
                List<Card> cards = new ArrayList<>();
                for (String s : listEntry.getValue())
                {
                    String[] cardTokens = s.split(",");
                    //first is suit, second is card (J Q 10 ...)
                    Card card = new Card(cardTokens[1], cardTokens[0]);
                    cards.add(card);
                }
                BlackJackEntity entity = null;
                if (className instanceof String)
                    switch ((String) className)
                    {
                        case "BlackJackAI":
                        {
                            entity = new BlackJackAI();
                            entity.setCards(cards);
                            entity.changeStatus(status);
                        }
                        break;
                        case "Dealer":
                        {
                            entity = new Dealer(null, null);
                            entity.setCards(cards);
                            entity.changeStatus(status);
                        }
                        break;
                        case "SplitPlayer":
                        {
                            entity = new SplitPlayer();
                            entity.setCards(cards);
                            entity.changeStatus(status);
                        }
                        break;
                        default:
                            System.out.println(className);
                            break;
                    }
                else if (className instanceof User)
                {
                    entity = new BlackJackPlayer((User) className);
                    entity.setCards(cards);
                    entity.changeStatus(status);
                }

                deserializedMap.put(entity, status);
            }
        }

        return deserializedMap;
    }

}
