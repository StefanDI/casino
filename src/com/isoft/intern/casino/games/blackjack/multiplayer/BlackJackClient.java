package com.isoft.intern.casino.games.blackjack.multiplayer;

import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.connection.ClientSocket;
import com.isoft.intern.view.UserInputHandler;
import com.isoft.intern.view.menus.BlackjackMenu;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import static com.isoft.intern.view.UserInputHandler.*;
import java.util.logging.Level;

import static com.isoft.intern.casino.games.roulette.state.context.RouletteContext.LOGGER;

/**
 * @author Stefan Ivanov
 */
public class BlackJackClient implements Runnable
{
    private final ClientSocket socket;
    private final BlackjackMenu view;
    private boolean isRunning;

    public BlackJackClient(ClientSocket socket,
                           BlackjackMenu view)
    {
        this.socket = socket;
        this.view = view;
        isRunning = true;
    }

    @Override
    public void run()
    {
        while (isRunning)
            waitForHost();
    }


    private void handleResponse(SocketResponse response) throws IOException, ClassNotFoundException
    {
        switch (response)
        {
            case WIN:
            {
                Double reward = (Double) socket.awaitServerResponse();
                view.gameEnded(BlackJackEntityStatus.WIN, reward);
            }
            break;
            case DRAW:
            {
                Double reward = (Double) socket.awaitServerResponse();
                view.gameEnded(BlackJackEntityStatus.DRAW, reward);
            }
            break;
            case LOSE:
            {
                view.gameEnded(BlackJackEntityStatus.LOSE, 0d);
            }
            break;
            case REFUND:
            {
                Double refundAmount = (Double) socket.awaitServerResponse();
                view.refund(refundAmount);
            }
            break;
            case REQUEST_EXPIRED:
            {
                view.requestExpired();
            }
            break;
            case INVALID_RESPONSE:
            {
                view.invalidResponse();
            }
            break;
        }
    }

    private void waitForHost()
    {
        try
        {
            Object response = socket.awaitServerResponse();
            System.out.println(response);
            if (response instanceof SocketRequest)
            {
                handleRequest((SocketRequest) response);
            } else if (response instanceof Map)
            {
                //noinspection unchecked
                view.printGame(
                        GameStateSerializer.deserializeGameState((
                                Map<Map<Object, List<String>>, BlackJackEntityStatus>) response));
            } else if (response instanceof SocketResponse)
            {
                handleResponse((SocketResponse) response);
            }
        } catch (IOException | ClassNotFoundException e)
        {
            socket.close();
            isRunning = false;
            writer.write(getString("io_socket_closed_error")+"\n");
            LOGGER.log(Level.WARNING, "IO exception and the socket is closed",e);
       }
    }

    private void handleRequest(SocketRequest request) throws IOException
    {
        switch (request)
        {
            case GET_USER:
                socket.writeToServer(view.getUser());
                break;
            case GET_BET_AMOUNT:
                socket.writeToServer(view.inputUserBet());
                break;
            case JOIN_REQUEST:
                SocketResponse resp = view.askUserToJoin();
                socket.writeToServer(resp);
                if (resp.equals(SocketResponse.CANCEL_JOIN))
                    isRunning = false;
                return;
            case REQUEST_USER_INPUT_AFTER_HIT:
            {
                socket.writeToServer(view.printOptions(false, false, false));
            }
            break;
            case REQUEST_USER_INPUT_INITIAL:
            {
                socket.writeToServer(view.printOptions(false, true, false));
            }
            break;
            case REQUEST_USER_INPUT_INITIAL_CAN_SPLIT:
            {
                socket.writeToServer(view.printOptions(true, true, false));
            }
            break;
            case REQUEST_USER_INPUT_INITIAL_HAS_SPLIT:
            {
                socket.writeToServer(view.printOptions(false, false, true));
            }
            break;
        }
    }
}

