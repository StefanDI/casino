package com.isoft.intern.casino.games.blackjack.multiplayer;

/**
 * interface is used as a callback to the method that send a request through a socket
 */
public interface OnSocketResponseCallback
{
    /**
     * this method is invoked when a socket responds wuth a response
     *
     * @param response the response from the socket
     */
    void response(Object response);
}
