package com.isoft.intern.casino.games.blackjack.multiplayer;

import java.io.Serializable;

/**
 * enum containing all responses for socket communications
 */
public enum SocketResponse implements Serializable
{
    CANCEL_JOIN,
    CONFIRM_JOIN,
    CONNECTION_LOST,
    DOUBLE,
    DRAW,
    HIT,
    HIT_SPLIT,
    INVALID_RESPONSE,
    LOSE,
    REFUND,
    REQUEST_EXPIRED,
    SPLIT,
    STAND,
    STAND_SPLIT, SURRENDER, WIN;
}
