package com.isoft.intern.casino.games.blackjack.multiplayer;


/**
 * enum containing all requests for socket communications
 */
public enum SocketRequest
{
    GET_USER,
    JOIN_REQUEST,
    GET_BET_AMOUNT,
    REQUEST_USER_INPUT_INITIAL,
    REQUEST_USER_INPUT_AFTER_HIT,
    REQUEST_USER_INPUT_INITIAL_CAN_SPLIT,
    REQUEST_USER_INPUT_INITIAL_HAS_SPLIT;
}
