package com.isoft.intern.casino.games.blackjack.multiplayer;

import com.isoft.intern.casino.games.blackjack.BlackJackGame;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.casino.games.blackjack.player.entities.BlackJackEntity;
import com.isoft.intern.casino.games.blackjack.player.entities.BlackJackPlayer;
import com.isoft.intern.connection.ClientSocket;
import com.isoft.intern.connection.ServerHost;
import com.isoft.intern.model.User;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;

import static com.isoft.intern.casino.games.roulette.state.context.RouletteContext.LOGGER;

/**
 * this class represents a blackjack host
 * its responsibility is to communicate with the player sockets
 * <br/>
 * under the hood it works with {@link ServerHost} and they communicate via the observer pattern
 * when a new socket has connected to the ServerHost, it notifies this object
 *
 * @author Stefan Ivanov
 */
public class BlackJackHost implements Observer
{
    private ServerHost host;
    private BlackJackGame game;

    public BlackJackHost(ServerHost host, BlackJackGame game)
    {
        this.host = host;
        this.game = game;

        host.addObserver(this);
    }

    public void run()
    {
        new Thread(host).start();
    }

    @Override
    public void update(Observable observable, Object o)
    {
        if (observable instanceof ServerHost)
            if (o instanceof ClientSocket)
                requestPlayer((ClientSocket) o);
    }

    /**
     * asks the socket for the user, if the socket responds with a wrong response
     * aks until appropriate response was received or connection was stopped
     *
     * @param socket the socket
     * @return true if the user was accepted, false if there was a socket connection error
     */
    private boolean requestPlayer(ClientSocket socket)
    {
        try
        {
            host.writeToSocket(socket, SocketRequest.GET_USER);
            User u = (User) host.listenToSocket(socket);
            BlackJackPlayer userPlayer = new BlackJackPlayer(u, socket);
            game.addPlayerToQueue(userPlayer);
            return true;
        } catch (IOException | IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,"IO exception or illegal access exception.",e);
            return false;
        } catch (ClassNotFoundException e)
        {
            LOGGER.log(Level.WARNING,"The socket responded with a wrong response message",e);
            try
            {
                //send a message to the socket that there was an unexpected response
                host.writeToSocket(socket, SocketResponse.INVALID_RESPONSE);
                //go into recursion, until the socket responds with the user, or breaks connection
                //and we go into the catch clause
                return requestPlayer(socket);
            } catch (IOException | IllegalAccessException ex)
            {
                //socket connection was destroyed even after n retries to ask for the player
                return false;
            }
        }
    }

    /**
     * sends a request to join the game in a separate thread
     *
     * @param player   the player to send the request to
     * @param callback the response of the socket
     */
    public void askToJoin(BlackJackPlayer player, OnSocketResponseCallback callback)
    {
        new Thread(() -> {
            try
            {
                host.writeToSocket(player.getSocket(), SocketRequest.JOIN_REQUEST);
                Object response = host.listenToSocket(player.getSocket());
                callback.response(response);
            } catch (IOException | IllegalAccessException | ClassNotFoundException e)
            {
                callback.response(SocketResponse.CANCEL_JOIN);
            }
        }).start();
    }

    /**
     * sends a request to a player for his bet in a separate thread
     *
     * @param player   the player
     * @param callback callback to the game for the response of the client
     */
    public void askForBet(BlackJackPlayer player, OnSocketResponseCallback callback)
    {
        new Thread(() -> {
            ClientSocket playerSocket = player.getSocket();
            try
            {
                host.writeToSocket(playerSocket, SocketRequest.GET_BET_AMOUNT);
                callback.response(host.listenToSocket(playerSocket));
            } catch (IOException | IllegalAccessException | ClassNotFoundException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
                callback.response(SocketResponse.CONNECTION_LOST);
            }

        }).start();
    }

    /**
     * sends a request for user input to the player
     *
     * @param player   the player
     * @param request  the request type(depends if the player has the option to double, split etc)
     * @param callback callback interface with the respond from the socket
     */
    public void askForInput(BlackJackPlayer player, SocketRequest request, OnSocketResponseCallback callback)
    {
        ClientSocket playerSocket = player.getSocket();
        try
        {
            host.writeToSocket(playerSocket, request);
            callback.response(host.listenToSocket(playerSocket));
        } catch (IOException | ClassNotFoundException | IllegalAccessException e)
        {
            callback.response(SocketResponse.CONNECTION_LOST);
        }

    }

    /**
     * builds the map representing the game state and send it to all sockets
     */
    public void sendRenderMapToAll()
    {
        //Order is important
        Map<BlackJackEntity, BlackJackEntityStatus> renderMap = new LinkedHashMap<>();
        renderMap.put(game.getDealer(), game.getDealer().getStatus());
        for (BlackJackPlayer player : game.getPlayers())
        {
            renderMap.put(player, player.getStatus());
            if (player.getHasSplit())
                renderMap.put(game.getSplit(player), game.getSplit(player).getStatus());
        }

        host.writeToAllSockets(GameStateSerializer.serializeGameState(renderMap));
    }

    /**
     * Sends request expired notification to the desired player
     * usually sent when the player answered too late
     *
     * @param player the player
     * @return true if success, false if there was a connection problem
     */
    public boolean requestExpired(BlackJackPlayer player)
    {
        try
        {
            host.writeToSocket(player.getSocket(), SocketResponse.REQUEST_EXPIRED);
            return true;
        } catch (IOException | IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,"Connection problem , request expired",e);
            return false;
        }
    }


    /**
     * notifies the player about an event
     * for now it is used only for reward, but in the future i may change double to object and reuse it
     *
     * @param player the player
     * @return true if success, false if there was a connection problem
     */
    public boolean notifyPlayer(BlackJackPlayer player, SocketResponse draw, Double reward)
    {
        try
        {
            host.writeToSocket(player.getSocket(), draw);
            host.writeToSocket(player.getSocket(), reward);
            return true;
        } catch (IllegalAccessException | IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            return false;
        }
    }
}
