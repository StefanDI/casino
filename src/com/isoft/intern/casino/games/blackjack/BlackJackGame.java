package com.isoft.intern.casino.games.blackjack;

import com.isoft.intern.casino.games.blackjack.enums.BlackJackEntityStatus;
import com.isoft.intern.casino.games.blackjack.enums.BlackJackGameState;
import com.isoft.intern.casino.games.blackjack.multiplayer.BlackJackHost;
import com.isoft.intern.casino.games.blackjack.multiplayer.SocketRequest;
import com.isoft.intern.casino.games.blackjack.multiplayer.SocketResponse;
import com.isoft.intern.casino.games.blackjack.player.entities.BlackJackAI;
import com.isoft.intern.casino.games.blackjack.player.entities.BlackJackPlayer;
import com.isoft.intern.casino.games.blackjack.player.entities.Dealer;
import com.isoft.intern.casino.games.blackjack.player.entities.SplitPlayer;
import com.isoft.intern.casino.games.blackjack.util.DeckGenerator;
import com.isoft.intern.connection.ServerHost;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Stefan Ivanov
 */
public class BlackJackGame implements Runnable
{
    private static final int NUMBER_OF_DECKS = 6;
    private static final int MAX_PLAYERS_PER_GAME = 3;
    private static final int JOIN_CONFIRMATION_TIME = 10000;
    private static final int ALLOW_BETS_TIME = 10000;

    private boolean isRunning;

    private Dealer dealer;
    private BlackJackGameState gameState;
    private List<BlackJackPlayer> players;

    private Map<BlackJackPlayer, BlackJackPlayer> splits;
    private Map<BlackJackPlayer, Double> playersByBet;

    private Queue<BlackJackPlayer> waitingQueue;
    private BlackJackHost host;
    private TurnHandler turnHandler;
    private int playersInQueue;
    private AtomicBoolean isPaused;


    /**
     * instantiates the server host and starts accepting sockets
     *
     * @throws IOException if the server host fails to instantiate
     */
    public BlackJackGame() throws IOException
    {
        isPaused = new AtomicBoolean(false);
        playersInQueue = 0;
        dealer = new Dealer(DeckGenerator.generateRandomDeckFrom(NUMBER_OF_DECKS), this);
        isRunning = true;
        gameState = BlackJackGameState.PREPARE;
        players = new ArrayList<>();
        waitingQueue = new ArrayDeque<>();
        turnHandler = new TurnHandler(this);
        splits = new HashMap<>();

        playersByBet = new HashMap<>();
        splits = new HashMap<>();

        host = new BlackJackHost(new ServerHost(7777), this);
        host.run();
    }

    /**
     * Tells the host to send the current game state to all the players
     *
     * @see BlackJackHost#sendRenderMapToAll() for details on how its done
     */
    public void render()
    {
        host.sendRenderMapToAll();
    }

    /**
     * Adds a player to the waiting queue
     * and logs the username of the player
     *
     * @param userPlayer the player to be added
     */
    public void addPlayerToQueue(BlackJackPlayer userPlayer)
    {
        System.out.println("Player joined queue: " + userPlayer.getPlayer().getUsername() + "\n");
        waitingQueue.add(userPlayer);
    }

    /**
     * getter for the players
     *
     * @return immutable list with all the players, currently playing
     */
    public List<? extends BlackJackPlayer> getPlayers()
    {
        return players;
    }


    @Override
    public void run()
    {
        while (isRunning)
        {
//            System.out.println("\nTICK\n");
//            try
//            {
//                Thread.sleep(1000);
//            } catch (InterruptedException e)
//            {
//                e.printStackTrace();
//            }
//            if (!players.isEmpty() || !waitingQueue.isEmpty())
            if (!isPaused.get())
                update();
        }
    }

    public Dealer getDealer()
    {
        return dealer;
    }

    /**
     * @return the map with players and their bets
     */
    public Map<BlackJackPlayer, Double> getPlayersByBet()
    {
        return this.playersByBet;
    }

    /**
     * Adds a split player to the player, this method is invoked whenever the player splits
     *
     * @param player the player that has split
     */
    public void addSplit(BlackJackPlayer player)
    {
        Card firstCard = player.getCards().get(0);
        Card secondCard = player.getCards().get(1);
        player.resetCards();
        player.addCard(firstCard);
        BlackJackPlayer splitPlayer = new SplitPlayer();
        splitPlayer.addCard(secondCard);
        splits.put(player, splitPlayer);
        render();
    }

    /**
     * return the split player of the desired player
     *
     * @param player the original user player
     * @return the split player object of that player
     */
    public SplitPlayer getSplit(BlackJackPlayer player)
    {

        return (SplitPlayer) splits.get(player);
    }


    /**
     * This is the loop of the blackjack game
     * switches between states and does appropriate action
     */
    public void update()
    {
        switch (gameState)
        {
            case PREPARE:
            {
                //This variable is necessary to check if all the players in the queue has accepted the request
                playersInQueue = waitingQueue.size();
                if (playersInQueue == 0)
                    return;
                Thread mainThread = Thread.currentThread();
                //Ask players to join
                askToJoin();

//                Thread checkIfAllUsersAccepted = new Thread(() -> {
//                    while (gameState.equals(BlackJackGameState.PREPARE))
//                    {
//                        if (players.size() >= MAX_PLAYERS_PER_GAME || players.size() == playersInQueue)
//                        {
//                            System.out.println("Main thread will be interrupted");
//                            mainThread.interrupt();
//                            break;
//                        }
//                    }
//                });
//                checkIfAllUsersAccepted.start();

                try
                {
                    Thread.sleep(JOIN_CONFIRMATION_TIME);
                    gameState = BlackJackGameState.BET;
                    System.out.println("\nENTERING BET STATE:");
                } catch (InterruptedException e)
                {
//                    e.printStackTrace();
                }


                //STOP running the main thread
//                isPaused.set(true);
            }
            break;
            case BET:
            {
                System.out.println("ASKING USERS FOR THEIR BETS\n");
                int playersToBeAskedForBets = players.size();
                //take bets from players
                askForBets();

                try
                {
                    Thread.sleep(ALLOW_BETS_TIME);
                    gameState = BlackJackGameState.DEAL;
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
//                Thread timer = new Thread(getTimer(ALLOW_BETS_TIME, () -> {
//                    System.out.println("DEALER DEALS CARDS");
//                    gameState = BlackJackGameState.DEAL;
////                    isPaused = false;
//                }));
//                timer.start();
//
//                new Thread(() -> {
//                    while (true)
//                    {
//                        //if all the players have placed their bets interrupt the timer
//                        if (playersByBet.size() == playersToBeAskedForBets)
//                        {
//                            timer.interrupt();
////                            isPaused = false;
//                            System.out.println("DEALER DEALS CARDS");
//                            gameState = BlackJackGameState.DEAL;
//                            break;
//                        }
//                    }
//                }).start();

//                isPaused = true;
            }
            case DEAL:
            {
                //remove all players who hasn't placed their bets
                players.clear();
                players.addAll(playersByBet.keySet());

                //FILL EMPTY PLACES WITH AI BOTS
                for (int i = players.size(); i < MAX_PLAYERS_PER_GAME; i++)
                {
                    players.add(new BlackJackAI());
                }


                dealer.dealHands();
                render();
                gameState = BlackJackGameState.PLAY;
            }
            break;
            case PLAY:
            {
                boolean isDealerBlackJack = false;
                if (dealer.hasAceFirstCard() && dealer.calcScore() == 21)
                {
                    isDealerBlackJack = true;
                    dealer.changeStatus(BlackJackEntityStatus.NATURAL);
                    dealer.showHoleCard();
                    render();
                    //DEALER state is BlackJack
                    //Continue with players
                }
                for (BlackJackPlayer player : players)
                {
                    if (player.hasBlackjack())
                    {
                        player.changeStatus(BlackJackEntityStatus.NATURAL);
                        render();
                    }
                }
                //If the dealer has blackjack, we have iterated over the players and figured out who has a natural too
                //we skip each player's turn and immediately evaluate
                if (isDealerBlackJack)
                    gameState = BlackJackGameState.EVALUATE;
                else
                {
                    for (BlackJackPlayer player : players)
                    {
                        if (player instanceof BlackJackAI)
                        {
                            ((BlackJackAI) player).takeTurn(turnHandler);
                            if (player.calcScore() == 21)
                                player.changeStatus(BlackJackEntityStatus.BLACKJACK);
                            else if (player.calcScore() > 21)
                                player.changeStatus(BlackJackEntityStatus.BUST);
                            render();
                        } else
                        {
                            if (!player.getStatus().equals(BlackJackEntityStatus.NATURAL))
                                takeTurn(player);
                        }
                    }

                    dealer.showHoleCard();
                    dealer.changeStatus(BlackJackEntityStatus.REVEAL_HOLE_CARD);
                    render();
                    dealer.takeTurn(this);
                }
                gameState = BlackJackGameState.EVALUATE;
            }
            break;
            case EVALUATE:
            {
                //iterating playersByBet because we ignore AI bot
                if (dealer.getStatus().equals(BlackJackEntityStatus.NATURAL))
                {
                    for (BlackJackPlayer player : playersByBet.keySet())
                    {
                        if (player.getStatus().equals(BlackJackEntityStatus.NATURAL))
                        {
                            boolean success =
                                    host.notifyPlayer(player, SocketResponse.DRAW, playersByBet.get(player));
                            if (!success)
                                player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                        } else
                        {
                            boolean success =
                                    host.notifyPlayer(player, SocketResponse.LOSE, playersByBet.get(player));
                            if (!success)
                                player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                        }
                    }
                } else
                {
                    for (BlackJackPlayer player : playersByBet.keySet())
                    {
                        double bet = playersByBet.get(player);
                        //If the player has won
                        if (player.calcScore() > dealer.calcScore() ||
                                dealer.getStatus().equals(BlackJackEntityStatus.BUST)
                                || dealer.calcScore() > 21)
                        {
                            if (player.getStatus().equals(BlackJackEntityStatus.NATURAL))
                            {
                                boolean success =
                                        host.notifyPlayer(player, SocketResponse.WIN, bet + bet * 1.5);
                                if (!success)
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                            } else if (player.getStatus().equals(BlackJackEntityStatus.DOUBLE))
                            {
                                boolean success =
                                        host.notifyPlayer(player, SocketResponse.WIN, bet + bet * 2);
                                if (!success)
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                            } else
                            {
                                boolean success =
                                        host.notifyPlayer(player, SocketResponse.WIN, bet * 2);
                                if (!success)
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                            }
                        } else if (player.calcScore() == dealer.calcScore())
                        {
                            boolean success =
                                    host.notifyPlayer(player, SocketResponse.DRAW, bet * 2);
                            if (!success)
                                player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                        }//else if the dealer wins, does not give them any reward
                    }
                }

                resetPlayers();

                if (isRunning)
                {
                    System.out.println("RESTARTING GAME\n");
                    gameState = BlackJackGameState.PREPARE;
                } else
                {
                    System.out.println("GAME STOPPED\n");
                    gameState = BlackJackGameState.STOP;
                }
            }
            break;
            case STOP:
            {
                return;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + gameState);
        }
    }


    private void resetPlayers()
    {
        for (BlackJackPlayer player : players)
        {
            if (!(player instanceof BlackJackAI))
            {
                if (player.getStatus().equals(BlackJackEntityStatus.DISCONNECTED))
                    continue;

                player.changeStatus(BlackJackEntityStatus.WAIT);
                player.setHasSplit(false);
                player.resetCards();
                waitingQueue.add(player);
            }
        }
        players.clear();
        dealer = new Dealer(DeckGenerator.generateRandomDeckFrom(NUMBER_OF_DECKS), this);
        splits.clear();
        playersByBet.clear();
    }

    private void askToJoin()
    {
        while (!waitingQueue.isEmpty() && players.size() < MAX_PLAYERS_PER_GAME)
        {
            BlackJackPlayer player = waitingQueue.poll();

            host.askToJoin(player, response -> {
                if (response.equals(SocketResponse.CONFIRM_JOIN))
                    if (gameState.equals(BlackJackGameState.PREPARE))
                    {
                        players.add(player);
                    } else
                    {
                        host.requestExpired(player);
                        waitingQueue.add(player);
                    }


            });


        }
    }

    private void askForBets()
    {
        for (BlackJackPlayer player : players)
        {
            if (player instanceof BlackJackAI)
                continue;

            host.askForBet(player, response -> {
                if (response instanceof SocketResponse &&
                        response.equals(SocketResponse.CONNECTION_LOST))
                {
                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                    return;
                }
                Double bet = (Double) response;
                if (gameState.equals(BlackJackGameState.BET))
                {
                    playersByBet.put(player, bet);
                } else
                {

                    if (host.requestExpired(player))
                    {
                        host.notifyPlayer(player, SocketResponse.REFUND, bet);
                        waitingQueue.add(player);
                    }

                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);

                }
            });
        }
    }

    private void takeTurn(BlackJackPlayer player)
    {
        //if this is true the while loop breaks
        //its used in lambda this is why its final and array
        final boolean[] shouldChangePlayer = {false};
        while (player.calcScore() <= 21)
        {
            if (shouldChangePlayer[0])
                break;
            else
            {
                if (player.canSplit())
                {
                    host.askForInput(player,
                            SocketRequest.REQUEST_USER_INPUT_INITIAL_CAN_SPLIT,
                            response -> {
                                SocketResponse res = (SocketResponse) response;

                                if (res.equals(SocketResponse.CONNECTION_LOST))
                                {
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                                    shouldChangePlayer[0] = true;
                                } else
                                    shouldChangePlayer[0] = turnHandler.handleTurn(res, player);

                            });
                } else if (player.getHasSplit())
                {
                    host.askForInput(player,
                            SocketRequest.REQUEST_USER_INPUT_INITIAL_HAS_SPLIT,
                            response -> {
                                SocketResponse res = (SocketResponse) response;

                                if (res.equals(SocketResponse.CONNECTION_LOST))
                                {
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                                    shouldChangePlayer[0] = true;
                                } else
                                    shouldChangePlayer[0] = turnHandler.handleTurn(res, player);

                            });

                } else if (player.getCards().size() == 2)
                {
                    host.askForInput(player,
                            SocketRequest.REQUEST_USER_INPUT_INITIAL,
                            response -> {
                                SocketResponse res = (SocketResponse) response;

                                if (res.equals(SocketResponse.CONNECTION_LOST))
                                {
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                                    shouldChangePlayer[0] = true;
                                } else
                                    shouldChangePlayer[0] = turnHandler.handleTurn(res, player);

                            });

                } else
                {
                    host.askForInput(player,
                            SocketRequest.REQUEST_USER_INPUT_AFTER_HIT,
                            response -> {
                                SocketResponse res = (SocketResponse) response;

                                if (res.equals(SocketResponse.CONNECTION_LOST))
                                {
                                    player.changeStatus(BlackJackEntityStatus.DISCONNECTED);
                                    shouldChangePlayer[0] = true;
                                } else
                                    shouldChangePlayer[0] = turnHandler.handleTurn(res, player);
                            });
                }

            }
        }
        if (player.calcScore() > 21)

        {
            player.changeStatus(BlackJackEntityStatus.BUST);
            render();
        } else if (player.calcScore() == 21)

        {
            player.changeStatus(BlackJackEntityStatus.BLACKJACK);
            render();
        }

    }

    private Runnable getTimer(long timeout, TimeoutCallback callback)
    {
        return () -> {
            try
            {
                Thread.sleep(timeout);
                callback.timerExpired();
            } catch (InterruptedException ignored)
            {
                //Timer was interrupted because it is no longer required
                //Just return without executing timerExpired()
            }
        };
    }

    /**
     * callback for getTimer after the timer has expired
     */
    private interface TimeoutCallback
    {
        void timerExpired();
    }
}