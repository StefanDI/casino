package com.isoft.intern.casino.games.blackjack.util;

import com.isoft.intern.casino.games.blackjack.Card;

import java.util.*;

/**
 * @author Stefan Ivanov
 */
public class DeckGenerator
{
    private static final int TOTAL_CARDS_IN_ONE_DECK = 52;

    private DeckGenerator()
    {
    }

    public static Queue<Card> generateRandomDeckFrom(int numberOfDecks)
    {
        //The Deque (pun intended) with all cards shuffled
        Queue<Card> cards = new ArrayDeque<>(numberOfDecks * TOTAL_CARDS_IN_ONE_DECK);

        //List of all the cards
        List<Card> allDecksStacked = new ArrayList<>();
        for (int i = 0; i < numberOfDecks; i++)
        {
            allDecksStacked.addAll(CardDeck.DECK);
        }

        Random r = new Random();
        while (allDecksStacked.size() > 0)
        {
            int deckCurrentSize = allDecksStacked.size();
            int randomCardIndex = r.nextInt(deckCurrentSize);
            cards.add(allDecksStacked.remove(randomCardIndex));
        }


        return cards;
    }
}
