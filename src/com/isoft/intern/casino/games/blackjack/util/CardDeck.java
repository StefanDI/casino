package com.isoft.intern.casino.games.blackjack.util;

import com.isoft.intern.casino.games.blackjack.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class CardDeck
{
    public static List<Card> DECK = new ArrayList<Card>()
    {{
        add(new Card(Card.CLUBS, "2"));
        add(new Card(Card.CLUBS, "3"));
        add(new Card(Card.CLUBS, "4"));
        add(new Card(Card.CLUBS, "5"));
        add(new Card(Card.CLUBS, "6"));
        add(new Card(Card.CLUBS, "7"));
        add(new Card(Card.CLUBS, "8"));
        add(new Card(Card.CLUBS, "9"));
        add(new Card(Card.CLUBS, "10"));
        add(new Card(Card.CLUBS, "J"));
        add(new Card(Card.CLUBS, "Q"));
        add(new Card(Card.CLUBS, "K"));
        add(new Card(Card.CLUBS, "A"));

        add(new Card(Card.DIAMONDS, "2"));
        add(new Card(Card.DIAMONDS, "3"));
        add(new Card(Card.DIAMONDS, "4"));
        add(new Card(Card.DIAMONDS, "5"));
        add(new Card(Card.DIAMONDS, "6"));
        add(new Card(Card.DIAMONDS, "7"));
        add(new Card(Card.DIAMONDS, "8"));
        add(new Card(Card.DIAMONDS, "9"));
        add(new Card(Card.DIAMONDS, "10"));
        add(new Card(Card.DIAMONDS, "J"));
        add(new Card(Card.DIAMONDS, "Q"));
        add(new Card(Card.DIAMONDS, "K"));
        add(new Card(Card.DIAMONDS, "A"));

        add(new Card(Card.HEARTS, "2"));
        add(new Card(Card.HEARTS, "3"));
        add(new Card(Card.HEARTS, "4"));
        add(new Card(Card.HEARTS, "5"));
        add(new Card(Card.HEARTS, "6"));
        add(new Card(Card.HEARTS, "7"));
        add(new Card(Card.HEARTS, "8"));
        add(new Card(Card.HEARTS, "9"));
        add(new Card(Card.HEARTS, "10"));
        add(new Card(Card.HEARTS, "J"));
        add(new Card(Card.HEARTS, "Q"));
        add(new Card(Card.HEARTS, "K"));
        add(new Card(Card.HEARTS, "A"));

        add(new Card(Card.SPADES, "2"));
        add(new Card(Card.SPADES, "3"));
        add(new Card(Card.SPADES, "4"));
        add(new Card(Card.SPADES, "5"));
        add(new Card(Card.SPADES, "6"));
        add(new Card(Card.SPADES, "7"));
        add(new Card(Card.SPADES, "8"));
        add(new Card(Card.SPADES, "9"));
        add(new Card(Card.SPADES, "10"));
        add(new Card(Card.SPADES, "J"));
        add(new Card(Card.SPADES, "Q"));
        add(new Card(Card.SPADES, "K"));
        add(new Card(Card.SPADES, "A"));
    }};


}
