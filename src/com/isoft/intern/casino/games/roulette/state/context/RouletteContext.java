package com.isoft.intern.casino.games.roulette.state.context;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.state.api.State;
import com.isoft.intern.casino.games.roulette.state.impl.*;

import java.util.logging.Logger;

/**
 *
 *
 * @author: Nikolay Uzunov
 */
public class RouletteContext
{
    public static final Logger LOGGER = Logger.getGlobal();
    private WithdrawState withdrawState;
    private SpinState spinState;
    private BetState betState;
    private WaitingState waitingState;
    private State currentState;
    private Roulette roulette;

    public RouletteContext(Roulette roulette)
    {
        this.roulette = roulette;
        this.withdrawState = new WithdrawState(this);
        this.spinState = new SpinState(this);
        this.betState = new BetState(this);
        this.waitingState = new WaitingState(this);
        currentState = waitingState;
    }

    public WaitingState getWaitingState()
    {
        return waitingState;
    }

    public WithdrawState getWithdrawState()
    {
        return withdrawState;
    }

    public SpinState getSpinState()
    {
        return spinState;
    }

    public BetState getBetState()
    {
        return betState;
    }

    public Roulette getRoulette()
    {
        return roulette;
    }

    public void setCurrentState(State currentState)
    {
        this.currentState = currentState;
    }

    /**
     * Starts the roulette as it passes through all the states
     */
    public void startRoulette()
    {
        currentState.standby(roulette);
        currentState.bet(roulette);
        currentState.spinRoulette(roulette);
        currentState.withdraw(roulette);
    }
}
