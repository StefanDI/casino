package com.isoft.intern.casino.games.roulette.state.api;

import com.isoft.intern.casino.games.roulette.Roulette;

/**
 * Roulette sates API
 *
 * @author Nikolay Uzunov
 */
public interface State
{
    void spinRoulette(Roulette roulette);
    void withdraw(Roulette roulette);
    void bet(Roulette roulette);
    void standby(Roulette roulette);
}
