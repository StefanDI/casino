package com.isoft.intern.casino.games.roulette.state.impl;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.state.api.State;
import com.isoft.intern.casino.games.roulette.state.context.RouletteContext;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.logging.Level;

/**
 * @author Nikolay Uzunov
 */
public class WaitingState implements State
{
    private RouletteContext context;

    public WaitingState(RouletteContext rouletteContext)
    {
        this.context = rouletteContext;
    }

    @Override
    public void spinRoulette(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    @Override
    public void withdraw(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    @Override
    public void bet(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    /**
     * If doesn't have players in the game the roulette goes in this state.
     * @param roulette holds the current roulette.
     */
    @Override
    public void standby(Roulette roulette)
    {

        while (roulette.getPlayers().isEmpty())
        {
            //RouletteContext.LOGGER.log(Level.INFO, "STATE: WAITING.");
            try
            {
                Thread.sleep(0);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        context.setCurrentState(context.getBetState());
    }
}
