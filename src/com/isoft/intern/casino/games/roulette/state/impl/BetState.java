package com.isoft.intern.casino.games.roulette.state.impl;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.bets.BetHolder;
import com.isoft.intern.casino.games.roulette.rouletteentities.Player;
import com.isoft.intern.casino.games.roulette.state.api.State;
import com.isoft.intern.casino.games.roulette.state.context.RouletteContext;
import com.isoft.intern.view.menus.RouletteMenu;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.logging.Level;

import static com.isoft.intern.view.UserInputHandler.userService;

/**
 * @author Nikolay Uzunov
 */
public class BetState implements State
{
    private RouletteContext context;

    public BetState(RouletteContext context)
    {
        this.context = context;
    }

    @Override
    public void spinRoulette(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    @Override
    public void withdraw(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    /**
     * Takes the bets from the player and updates it.
     * @param roulette holds the current roulette.
     */
    @Override
    public void bet(Roulette roulette)
    {
        List<Player> players = roulette.getPlayers();
        RouletteMenu menu;
        double moneyBeforeBetting;

        for (Player player : players)
        {
            roulette.setCurrentPlayer(player);
            menu = player.getMenu();
            menu.visualizeBetMenu(roulette.getVisualizer());
            roulette.bet();
            moneyBeforeBetting = player.getAvailableMoney();
            player.setAvailableMoney(moneyBeforeBetting - BetHolder.totalBet);
            userService.updateUser(player);
            BetHolder.clearTotalBet();
        }
        context.setCurrentState(context.getSpinState());
    }

    @Override
    public void standby(Roulette roulette)
    {
        throw new NotImplementedException();
    }
}
