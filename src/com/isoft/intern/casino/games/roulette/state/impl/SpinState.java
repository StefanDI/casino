package com.isoft.intern.casino.games.roulette.state.impl;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.rouletteentities.Dealer;
import com.isoft.intern.utils.gameloader.Loader;
import com.isoft.intern.casino.games.roulette.state.api.State;
import com.isoft.intern.casino.games.roulette.state.context.RouletteContext;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.logging.Level;

/**
 * @author Nikolay Uzunov
 */
public class SpinState implements State
{
    private RouletteContext context;

    public SpinState(RouletteContext context)
    {
        this.context = context;
    }

    /**
     * The dealer in the roulette spins the roulette and returns the winning number.
     * @param roulette holds the current roulette.
     */
    @Override
    public void spinRoulette(Roulette roulette)
    {
        Dealer dealer = roulette.getDealer();
        roulette.setWinningNumber(dealer.spinRoulette());
        context.setCurrentState(context.getWithdrawState());
        Loader.load();
        //Loader.load();
    }

    @Override
    public void withdraw(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    @Override
    public void bet(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    @Override
    public void standby(Roulette roulette)
    {
        throw new NotImplementedException();
    }
}
