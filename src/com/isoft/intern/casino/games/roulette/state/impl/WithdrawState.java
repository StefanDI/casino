package com.isoft.intern.casino.games.roulette.state.impl;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.bets.RouletteBet;
import com.isoft.intern.casino.games.roulette.bets.RouletteNumbers;
import com.isoft.intern.casino.games.roulette.state.api.State;
import com.isoft.intern.casino.games.roulette.state.context.RouletteContext;
import com.isoft.intern.model.User;
import com.isoft.intern.userholder.LoggedInUser;
import com.isoft.intern.view.menus.RouletteMenu;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Map;
import java.util.logging.Level;

import static com.isoft.intern.view.UserInputHandler.userService;

/**
 * @author Nikolay Uzunov
 */
public class WithdrawState implements State
{
    private RouletteContext context;

    public WithdrawState(RouletteContext context)
    {
        this.context = context;
    }

    @Override
    public void spinRoulette(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    /**
     * Displays the result of game and updates the player.
     * And the game pass to the next state.
     * If player wants to exit from game and maps with players are empty
     * the next state is "Waiting", otherwise the next state is "Bet".
     * If you wants to play again the state is "Bet" too.
     * @param roulette holds the current roulette.
     */
    @Override
    public void withdraw(Roulette roulette)
    {
        RouletteNumbers winningNumber = roulette.getWinningNumber();
        RouletteMenu menu = roulette.getCurrentPlayer().getMenu();
        Map<RouletteBet, Double> winningBets = roulette.getWinningBets(winningNumber);
        User user = LoggedInUser.getInstance().getCurrentlyLoggedUser();
        double reward = 0;

        for (Map.Entry<RouletteBet, Double> rouletteBetReward : winningBets.entrySet())
        {
            reward += rouletteBetReward.getValue();
        }
        menu.printWinningBets(winningBets);
        user.setAvailableMoney(user.getAvailableMoney() + reward);
        userService.updateUser(user);
        roulette.clearBets();
        if (menu.hasQuit())
        {
            //Loader.getInstance().interrupt();
            menu.playerExit();
            if (roulette.getPlayers().isEmpty())
            {
                context.setCurrentState(context.getWaitingState());
            }
        }
        else
        {
            context.setCurrentState(context.getBetState());
        }
    }

    @Override
    public void bet(Roulette roulette)
    {
        throw new NotImplementedException();
    }

    @Override
    public void standby(Roulette roulette)
    {
        throw new NotImplementedException();
    }
}
