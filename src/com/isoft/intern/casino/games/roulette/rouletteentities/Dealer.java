package com.isoft.intern.casino.games.roulette.rouletteentities;

import com.isoft.intern.casino.games.roulette.Roulette;
import com.isoft.intern.casino.games.roulette.bets.RouletteNumbers;

/**
 * @author Nikolay Uzunov
 */
public class Dealer
{
    private Roulette roulette;

    public Dealer(Roulette roulette)
    {
        this.roulette = roulette;
    }

    /**
     * This method serves to simulates roulette rotation.
     * @return roulette number;
     */
    public RouletteNumbers spinRoulette()
    {
        return this.roulette.generateResult();
    }
}
