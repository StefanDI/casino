package com.isoft.intern.casino.games.roulette.rouletteentities;

import com.isoft.intern.casino.games.roulette.bets.RouletteBet;
import com.isoft.intern.casino.games.roulette.visualizer.Visualizer;
import com.isoft.intern.model.User;
import com.isoft.intern.view.menus.RouletteMenu;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Nikolay Uzunov
 */
public class Player extends User
{
    private volatile RouletteMenu menu;
    private Map<RouletteBet, Double> placedBets;
    private Map<Integer, Double> placedBetsOnNumber;
    public Player(User user)
    {
        super(user);
        placedBets = new HashMap<>();
        placedBetsOnNumber = new HashMap<>();
    }

    public void setPlayer(User user)
    {
        setFirstName(user.getFirstName());
        setMiddleName(user.getMiddleName());
        setLastName(user.getLastName());
        setAge(user.getAge());
        setIdNumber(user.getIdNumber());
        setEmail(user.getEmail());
        setUsername(user.getUsername());
        setPassword(user.getPassword());
        setLocked(user.isLocked());
        setUserRole(user.getUserRole());
        setAvailableMoney(user.getAvailableMoney());
    }

    public RouletteMenu getMenu()
    {
        return menu;
    }

    public void setMenu(RouletteMenu menu)
    {
        this.menu = menu;
    }

    public Map<RouletteBet, Double> getPlacedBets()
    {
        return placedBets;
    }

    public Map<Integer, Double> getPlacedBetsOnNumber()
    {
        return placedBetsOnNumber;
    }

    /**
     * This method serves to add a new bet, difference of number into the player's bet list.
     * @param rouletteBet holds the type of bet
     * @param bet holds the money.
     */
    public void placeBet(RouletteBet rouletteBet, double bet)
    {
       this.placedBets.put(rouletteBet, bet);
    }

    /**
     * This method serves to add a new bet, placed on number into the player's bet list
     * @param number holds the bet number.
     * @param bet holds the money.
     */
    public void placeBet(double bet, int number)
    {
        if (number < 0 || number > 36)
        {
            return;
        }
        this.placedBetsOnNumber.put(number, bet);
    }

    /**
     * This method serves to clear the Map sets, which holds player's bets.
     */
    public void clearBets()
    {
        placedBetsOnNumber.clear();
        placedBets.clear();
    }

    /**
     * Check whether the map sets are empty or not.
     * @return <tt>true</tt> if maps is not empty.
     */
    public boolean hasBets()
    {
        return !placedBets.isEmpty() || !placedBetsOnNumber.isEmpty();
    }

    @Override
    public boolean equals(Object obj)
    {
        return obj instanceof Player && ((Player) obj).getUsername().equals(this.getUsername());
    }
}
