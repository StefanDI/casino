package com.isoft.intern.casino.games.roulette.bets;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nikolay Uzunov
 * This class holds bets for current Player.
 */
public class BetHolder
{
    public static double bet;
    public static double totalBet = 0;
    public static boolean betSuccess;
    private static Map<RouletteBet, Double> rouletteBets = new HashMap<>();
    private static Map<Integer, Double> betsOnNumber = new HashMap<>();

    public static Map<RouletteBet, Double> getRouletteBets()
    {
        return rouletteBets;
    }

    public static Map<Integer, Double> getBetsOnNumber()
    {
        return betsOnNumber;
    }

    /**
     * This method serves to add a new bet, difference of number into the player's bet list.
     * @param rouletteBet holds the type of bet
     * @param bet holds the money.
     */
    public static void setBet(RouletteBet rouletteBet, double bet)
    {
        rouletteBets.put(rouletteBet, bet);
        totalBet += bet;
    }

    /**
     * This method serves to add a new bet, placed on number into the player's bet list
     * @param number holds the bet number.
     * @param bet holds the money.
     */
    public static void setBet(int number, double bet)
    {
        betsOnNumber.put(number, bet);
        totalBet += bet;
    }

    /**
     * This method serves to clear the Map sets, which holds player's bets.
     */
    public static void clearHolder()
    {
        betsOnNumber.clear();
        rouletteBets.clear();
    }

    public static void clearTotalBet()
    {
        totalBet = 0;
    }

    /**
     * Check if the maps are empty return true/false.
     * @return if they are not empty return true otherwise return false
     */
    public static boolean hasBets()
    {
        if (rouletteBets.isEmpty() && getBetsOnNumber().isEmpty())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
