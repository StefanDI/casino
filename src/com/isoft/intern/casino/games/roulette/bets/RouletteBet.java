package com.isoft.intern.casino.games.roulette.bets;

/**
 * This class holds all bet types.
 * @author Nikolay Uzunov
 */
public enum RouletteBet
{
    RED(1), BLACK(1),
    NUMBER(35),
    ODD(1), EVEN(1),
    FIRST_DOZEN(2),SECOND_DOZEN(2),THIRD_DOZEN(2),
    LOW(1),HIGH(1);
    private final double reward;

    RouletteBet(double reward)
    {
        this.reward = reward;
    }

    /**
     * Returns a number to multiply the bet
     * @return
     */
    public double getRewardMultiplier()
    {
        return this.reward;
    }
}
