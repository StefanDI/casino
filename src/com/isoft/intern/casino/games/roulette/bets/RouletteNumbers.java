package com.isoft.intern.casino.games.roulette.bets;

public enum RouletteNumbers
{
    //DO NOT PRETTIFY
    ZERO       (0),
    ONE        (1,  RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    TWO        (2,  RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    THREE      (3,  RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    FOUR       (4,  RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    FIVE       (5,  RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    SIX        (6,  RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    SEVEN      (7,  RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    EIGHT      (8,  RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    NINE       (9,  RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    TEN        (10, RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    ELEVEN     (11, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    TWELVE     (12, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.FIRST_DOZEN, RouletteBet.LOW),
    THIRTEEN   (13, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.SECOND_DOZEN,RouletteBet.LOW),
    FOURTEEN   (14, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.SECOND_DOZEN,RouletteBet.LOW),
    FIFTEEN    (15, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.SECOND_DOZEN,RouletteBet.LOW),
    SIXTEEN    (16, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.SECOND_DOZEN,RouletteBet.LOW),
    SEVENTEEN  (17, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.SECOND_DOZEN,RouletteBet.LOW),
    EIGHTEEN   (18, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.SECOND_DOZEN,RouletteBet.LOW),
    NINETEEN   (19, RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.SECOND_DOZEN,RouletteBet.HIGH),
    TWENTY     (20, RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.SECOND_DOZEN,RouletteBet.HIGH),
    TWENTY_ONE(21, RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.SECOND_DOZEN,RouletteBet.HIGH),
    TWENTY_TWO(22, RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.SECOND_DOZEN,RouletteBet.HIGH),
    TWENTY_THREE(23, RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.SECOND_DOZEN,RouletteBet.HIGH),
    TWENTY_FOUR(24, RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.SECOND_DOZEN,RouletteBet.HIGH),
    TWENTY_FIVE(25, RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    TWENTY_SIX(26, RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    TWENTY_SEVEN(27, RouletteBet.RED,   RouletteBet.ODD,  RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    TWENTY_EIGHT(28, RouletteBet.BLACK, RouletteBet.EVEN, RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    TWENTY_NINE(29, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY     (30, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY_ONE(31, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY_TWO(32, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY_THREE(33, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY_FOUR(34, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY_FIVE(35, RouletteBet.BLACK, RouletteBet.ODD,  RouletteBet.THIRD_DOZEN, RouletteBet.HIGH),
    THIRTY_SIX(36, RouletteBet.RED,   RouletteBet.EVEN, RouletteBet.THIRD_DOZEN, RouletteBet.HIGH);

    public int number;
    public RouletteBet[] wins;

    RouletteNumbers(int number, RouletteBet... wins)
    {
        this.number = number;
        this.wins = wins;
    }
}
