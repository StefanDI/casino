package com.isoft.intern.casino.games.roulette.visualizer;

import com.isoft.intern.utils.ANSI;

/**
 * This implementation provides all operations for visualizing a roulette game.
 *
 * @author: Nikolay Uzunov

 */
public class Visualizer
{
    private static final int ROULETTE_NUMBER_ROWS = 3;
    private static final int ROULETTE_NUMBER_COLS = 13;
    private static final int WHOLE_ROULETTE_ROWS = ROULETTE_NUMBER_ROWS + 3;
    private static final int WHOLE_ROULETTE_COLS = ROULETTE_NUMBER_COLS + 3;

    private static final int LOW_X = 0;
    private static final int LOW_Y = 1;
    private static final int HIGH_X = 0;
    private static final int HIGH_Y = 2;
    private static final int BLACK_X = 5;
    private static final int BLACK_Y = 3;
    private static final int ODD_X = 5;
    private static final int ODD_Y = 4;
    private static final int RED_X = 5;
    private static final int RED_Y = 2;
    private static final int EVEN_X = 5;
    private static final int EVEN_Y = 1;
    private static final int DOZEN_ONE_X = 4;
    private static final int DOZEN_ONE_Y = 1;
    private static final int DOZEN_TWO_X = 4;
    private static final int DOZEN_TWO_Y = 2;
    private static final int DOZEN_THREE_X = 4;
    private static final int DOZEN_THREE_Y = 3;

    private static final String BET_HIGHLIGHTER = ANSI.ANSI_UNDERLINE + ANSI.ANSI_BOLD;

    private static String[][] wholeRoulette = new String[WHOLE_ROULETTE_ROWS][WHOLE_ROULETTE_COLS];
    private static String[][] rouletteNumbers = new String[ROULETTE_NUMBER_ROWS][ROULETTE_NUMBER_COLS];

    public Visualizer()
    {
        makeRoulette();
    }

    /**
     * Prints the roulette visualization and all bet types
     * @param text
     */
    public void printRouletteAnd(String[][] text)
    {
        for (int i = 0; i < WHOLE_ROULETTE_ROWS; i++)
        {
            int i1;
            for (i1 = 0; i1 < wholeRoulette[i].length; i1++)
            {
                if (wholeRoulette[i][i1] != null)
                    System.out.print(wholeRoulette[i][i1]);
            }
            if (i < text.length)
                for (int j = 0; j < text[i].length; j++)
                {
                    System.out.print(text[i][j]);
                }

            System.out.println();
        }
    }

    /*public void printRoulette()
    {
        for (int i = 0; i < WHOLE_ROULETTE_ROWS; i++)
        {
            for (int i1 = 0; i1 < wholeRoulette[i].length; i1++)
            {
                if (wholeRoulette[i][i1] != null)
                    System.out.print(wholeRoulette[i][i1]);
            }
            System.out.println();
        }
    }

    public void betOnNumber(int number)
    {
        if (number == 0)
        {
            wholeRoulette[2][0] = BET_HIGHLIGHTER + wholeRoulette[2][0];
            return;
        }
        int[] pos = getPosition(number);
        if (pos != null)
            wholeRoulette[pos[0]][pos[1]] = BET_HIGHLIGHTER + wholeRoulette[pos[0]][pos[1]];
    }

    public void betHigh()
    {
        wholeRoulette[HIGH_X][HIGH_Y] = BET_HIGHLIGHTER + wholeRoulette[HIGH_X][HIGH_Y];
    }

    public void betLow()
    {
        wholeRoulette[LOW_X][LOW_Y] = BET_HIGHLIGHTER + wholeRoulette[LOW_X][LOW_Y];
    }

    public void betBlack()
    {
        wholeRoulette[BLACK_X][BLACK_Y] = BET_HIGHLIGHTER + wholeRoulette[BLACK_X][BLACK_Y];
    }


    public void betRed()
    {
        wholeRoulette[RED_X][RED_Y] = BET_HIGHLIGHTER + wholeRoulette[RED_X][RED_Y];
    }

    public void betEven()
    {
        wholeRoulette[EVEN_X][EVEN_Y] = BET_HIGHLIGHTER + wholeRoulette[EVEN_X][EVEN_Y];
    }

    public void betDozenOne()
    {
        wholeRoulette[DOZEN_ONE_X][DOZEN_ONE_Y] = BET_HIGHLIGHTER + wholeRoulette[DOZEN_ONE_X][DOZEN_ONE_Y];
    }


    public void betDozenTwo()
    {
        wholeRoulette[DOZEN_TWO_X][DOZEN_TWO_Y] = BET_HIGHLIGHTER + wholeRoulette[DOZEN_TWO_X][DOZEN_TWO_Y];
    }

    public void betDozenThree()
    {
        wholeRoulette[DOZEN_THREE_X][DOZEN_THREE_Y] = BET_HIGHLIGHTER + wholeRoulette[DOZEN_THREE_X][DOZEN_THREE_Y];
    }

    public void betOdd()
    {
        wholeRoulette[ODD_X][ODD_Y] = BET_HIGHLIGHTER + wholeRoulette[ODD_X][ODD_Y];
    }*/

    private void makeRoulette()
    {
        generateRouletteNumbers();
        addSpaces();
        //LOW
        wholeRoulette[0][1] = ANSI.ANSI_BRIGHT_BG_GREEN + ANSI.ANSI_BLACK + getString("H) 1 to 18", 21);
        //HIGH
        wholeRoulette[0][2] = ANSI.ANSI_BG_GREEN + ANSI.ANSI_BLACK + getString("I) 19 to 36", 24);
        //ADD ROULETTE NUMBERS TO MATRIX
        System.arraycopy(rouletteNumbers, 0, wholeRoulette, 1, WHOLE_ROULETTE_ROWS - 2 - 1);
        //FIRST DOZEN
        wholeRoulette[WHOLE_ROULETTE_ROWS - 2][1] = ANSI.ANSI_BRIGHT_BG_GREEN + ANSI.ANSI_BLACK + getString("A) 1 to 12", 13);
        //SECOND DOZEN
        wholeRoulette[WHOLE_ROULETTE_ROWS - 2][2] = ANSI.ANSI_BG_GREEN + ANSI.ANSI_BLACK + getString("B) 13 to 24", 16);
        //THIRD DOZEN
        wholeRoulette[WHOLE_ROULETTE_ROWS - 2][3] = ANSI.ANSI_BRIGHT_BG_GREEN + ANSI.ANSI_BLACK + getString("C) 25 to 36", 16);
        //EVEN
        wholeRoulette[WHOLE_ROULETTE_ROWS - 1][1] = ANSI.ANSI_BG_GREEN + ANSI.ANSI_BLACK + getString("D) EVEN", 9);
        //RED
        wholeRoulette[WHOLE_ROULETTE_ROWS - 1][2] = ANSI.ANSI_BG_RED + getString("E) RED", 13);
        //BLACK
        wholeRoulette[WHOLE_ROULETTE_ROWS - 1][3] = ANSI.ANSI_BG_BLACK + getString("F) BLACK", 14);
        //ODD
        wholeRoulette[WHOLE_ROULETTE_ROWS - 1][4] = ANSI.ANSI_BG_GREEN + ANSI.ANSI_BLACK + getString("G) ODD", 9);
    }

    private void addSpaces()
    {
        for (int i = 0; i < wholeRoulette.length; i++)
        {
            if (i >= 1 && i < WHOLE_ROULETTE_ROWS - 2)
                continue;
            wholeRoulette[i][0] = "   ";
        }
    }

    private void generateRouletteNumbers()
    {
        int currentNumber = 1;
        for (int col = 0; col < ROULETTE_NUMBER_COLS; col++)
        {
            for (int row = ROULETTE_NUMBER_ROWS - 1; row >= 0; row--)
            {
                if (col == 0 && row == 1)
                    rouletteNumbers[row][col] = ANSI.ANSI_BG_GREEN + ANSI.ANSI_BLACK + " " + 0 + " " + ANSI.ANSI_RESET;
                else if (col == 0)
                    rouletteNumbers[row][col] = " " + ANSI.ANSI_BG_GREEN + "  ";
                else
                {
                    rouletteNumbers[row][col] = getNumber(currentNumber++);
                }
            }
        }
    }

    /*private int[] getPosition(int number)
    {
        for (int i = 0; i < wholeRoulette.length; i++)
        {
            for (int i1 = 0; i1 < wholeRoulette[i].length; i1++)
            {
                if (wholeRoulette[i][i1] != null && wholeRoulette[i][i1].length() > 3)
                {
                    String num = wholeRoulette[i][i1];
                    num = num.substring(5);
                    num = num.substring(0, 3);
                    if (num.trim().equals(String.valueOf(number)))
                        return new int[]{i, i1};
                }
            }
        }
        return null;
    }*/

    private String getString(String text, int totalSpaces)
    {
        int textToFill = totalSpaces - text.length();
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < textToFill / 2; i++)
        {
            line.append(" ");
        }
        line.append(text);
        for (int i = 0; i < textToFill - textToFill / 2; i++)
        {
            line.append(" ");
        }

        return line + ANSI.ANSI_RESET;
    }

    private String getNumber(int number)
    {
        String coloredNumber;
        String extraSpace = " ";
        if ((number >= 1 && number <= 10) || (number >= 19 && number < 30))
        {
            if (number % 2 == 0)
                coloredNumber = ANSI.ANSI_BG_BLACK + extraSpace + number + extraSpace + ANSI.ANSI_RESET;
            else
                coloredNumber = ANSI.ANSI_BG_RED + extraSpace + number + extraSpace + ANSI.ANSI_RESET;
        } else
        {
            if (number % 2 != 0)
                coloredNumber = ANSI.ANSI_BG_BLACK + extraSpace + number + extraSpace + ANSI.ANSI_RESET;
            else
                coloredNumber = ANSI.ANSI_BG_RED + extraSpace + number + extraSpace + ANSI.ANSI_RESET;
        }

        return coloredNumber;
    }
}
