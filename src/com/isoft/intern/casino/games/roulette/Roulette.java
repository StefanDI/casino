package com.isoft.intern.casino.games.roulette;

import com.isoft.intern.casino.games.roulette.bets.BetHolder;
import com.isoft.intern.casino.games.roulette.bets.RouletteBet;
import com.isoft.intern.casino.games.roulette.bets.RouletteNumbers;
import com.isoft.intern.casino.games.roulette.rouletteentities.Dealer;
import com.isoft.intern.casino.games.roulette.rouletteentities.Player;
import com.isoft.intern.casino.games.roulette.state.context.RouletteContext;
import com.isoft.intern.casino.games.roulette.visualizer.Visualizer;
import com.isoft.intern.userholder.LoggedInUser;

import java.util.*;

/**
 * @author Nikolay Uzunov
 */
public class Roulette extends Thread
{
    private final Visualizer visualizer = new Visualizer();
    public RouletteContext context;
    public boolean isRunning = true;
    private List<Player> players;
    private Player currentPlayer;
    private Dealer dealer;
    private RouletteNumbers winningNumber;

    public Roulette()
    {
        this.context = new RouletteContext(this);
        this.players = new ArrayList<>();
        this.dealer = new Dealer(this);
    }

    public RouletteNumbers getWinningNumber()
    {
        return winningNumber;
    }

    public void setWinningNumber(RouletteNumbers winningNumber)
    {
        this.winningNumber = winningNumber;
    }

    public Dealer getDealer()
    {
        return this.dealer;
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public Player getCurrentPlayer()
    {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer)
    {
        this.currentPlayer = currentPlayer;
    }

    public Visualizer getVisualizer()
    {
        return this.visualizer;
    }

    @Override
    public void run()
    {
        while (isRunning)
        {
            context.startRoulette();
        }
    }

    /**
     * Add new player in List of players.
     * @param player hols the player which will be added.
     */
    public void join(Player player)
    {
        players.add(player);
    }

    /**
     * Remove the logged in player.
     */
    public void removePlayer()
    {
        Player player = new Player(LoggedInUser.getInstance().getCurrentlyLoggedUser());
        players.remove(player);
    }

    /**
     * Generate random number, that is used for a roulette result.
     * @return random Integer number.
     */
    public RouletteNumbers generateResult()
    {
        Random random = new Random();
        RouletteNumbers[] numbers = RouletteNumbers.values();

        return numbers[random.nextInt(numbers.length)];
    }

    /**
     * Takes the current player and clear his bets.
     */
    public void clearBets()
    {
        currentPlayer.clearBets();
    }

    /**
     * Takes the winning number as a parameter and finds all winning bets.
     * @param winningNumber random generated Roulette number from {@link #generateResult()}
     * @return map with the winning bets and the money player has bet on
     */
    public Map<RouletteBet, Double> getWinningBets(RouletteNumbers winningNumber)
    {
        Map<RouletteBet, Double> winningBets = new HashMap<>();
        for (RouletteBet win : winningNumber.wins)
        {
            if (currentPlayer.getPlacedBets().containsKey(win))
            {
                double placedMoney = currentPlayer.getPlacedBets().get(win);
                double wonFromBet = placedMoney + placedMoney * win.getRewardMultiplier();
                winningBets.put(win, wonFromBet);
            }
            if (currentPlayer.getPlacedBetsOnNumber().containsKey(winningNumber.number))
            {
                double placedMoney = currentPlayer.getPlacedBetsOnNumber().get(winningNumber.number);
                double wonFromBet = placedMoney + placedMoney * RouletteBet.NUMBER.getRewardMultiplier();
                winningBets.put(RouletteBet.NUMBER, wonFromBet);
            }
        }
        return winningBets;
    }

    /**
     * Takes all bets from {@link BetHolder} and placed on the player's bets.
     */
    public void bet()
    {
        Map<RouletteBet, Double> rouletteBets = BetHolder.getRouletteBets();
        Map<Integer, Double> betsOnNumber = BetHolder.getBetsOnNumber();

        for (RouletteBet key : rouletteBets.keySet())
        {
            bet(key, rouletteBets.get(key));
        }

        for (Integer key : betsOnNumber.keySet())
        {
            betNumber(key, betsOnNumber.get(key));
        }
        BetHolder.clearHolder();
    }

    /**
     * Add bet in player's map of bets.
     * if he is placed on Number.
     * @param num holds the bet number.
     * @param bet holds money he wants to bet.
     */
    private void betNumber(Integer num, double bet)
    {
        currentPlayer.placeBet(bet, num);
    }

    /**
     * Add bet in player's map of bets.
     * if he is placed on bet type.
     * @param num holds the bet number.
     * @param bet holds money he wants to bet.
     */
    private void bet(RouletteBet betType, double bet)
    {
        currentPlayer.placeBet(betType, bet);
    }
}