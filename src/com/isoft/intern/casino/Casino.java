package com.isoft.intern.casino;

import com.isoft.intern.casino.games.roulette.Roulette;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Uzunov
 */
public class Casino
{
    private static final int ROULETTE_COUNT = 1;
    private List<Roulette> rouletteArrayList = new ArrayList<>();

    public Casino()
    {
        addRoulette();
    }

    private void addRoulette()
    {
        for (int i = 0; i < ROULETTE_COUNT; i++)
        {
            Roulette roulette = new Roulette();
            rouletteArrayList.add(roulette);
            new Thread(roulette).start();
        }
    }

    public List<Roulette> getRouletteArrayList()
    {
        return rouletteArrayList;
    }
}
